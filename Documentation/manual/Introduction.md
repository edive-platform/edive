# eDIVE Development Documentation

This part of the documentation is written primarily for software developers.
* If you want to learn more about our concept of using VR in education, instructional design, etc, check [our website (WIP)](https://eduincive.muni.cz/).
* If your are looking for information how to setup or run existing eDIVE scenarios, check [For Users](User_Manual.md) section.


## How to start developing using eDIVE?

1. If not familiar with eDIVE platform yet, we recommend to read through short [Description](Description.md) of the system, its architecture and features. Also, [For Users](./User_Manual.md) section could provide you useful insight.
2. Use the [First-time Project Setup](./Setup.md) guide to setup new eDIVE Unity project on your machine.
3. Try to modify/extend one of existing modules. Or create a new one directly.

## Hints
* Please use `namespace`s to structure your code. You can check other classes for reference.
* Please, continuously document your source codes, according to our [Documentation Guide](Documenting.md). 

