# Project Setup

## Requirements
### Unity
eDIVE is based on Unity game engine. You will need Unity engine installed, to be able to work with eDIVE. See [Unity.com](https://unity.com/) for downloads, licences and more.

For now, eDIVE is aligned with specific version: **Unity 2020.3.19**.

This version can be obtained through [Unity Hub](https://unity.com/download) (recommended approach) or directly at [Unity Download Archive](https://unity.com/releases/editor/archive).

Soon, the eDIVE project will be updated to newer version of Unity.

### IDE
You will need also some development environment. Unity supports Microsoft Visual Studio ("full" as well as "code") and also Jetbrains Rider so it is up to your personal preferences. Both these IDEs has good integrations with Unity editor.

### git

For downloading the source codes, you will need a git version control system. If you are not familiar with it, see [git](https://git-scm.com/).

### VR Headset

For developing VR experience, some VR headset is necessary (technically speaking, you can develop even without it, but the testing is nearly impossible). 

Currently, the project is set-up for use with **Meta Quest** headsets (both the first and the second generation works fine). Due to Unity's support of various XR devices, it is possible to adjust project to other headsets. 

### Mirror
Mirror is a open-source, high level networking library for Unity. See [mirror-networking.com](https://mirror-networking.com/) for details. eDIVE is using Mirror as basis for its own networking layer.

##  Source Codes
eDIVE  project is open-source, therefore you can use it freely for non-commercial usage. Check [Readme](https://gitlab.fi.muni.cz/grp-edive/edive-platform/-/blob/main/README.md) file for more details.

Source codes are hosted at: [gitlab.fi.muni.cz](https://gitlab.fi.muni.cz/grp-edive/edive-platform).

The eDIVE is based on modular architecture. This repository holds only the "Core" module. Additional modules are kept in separate repositories and can be obtained directly in Unity - see following section.  

 

> [!TIP]
> We recommend to *fork* the eDIVE repository and work on your own fork. In this way, you can freely experiment with the system, do any changes and keep versioning using git. And later, possibly, use *merge request* to integrate your feature into public master repository.

## Initial Project Setup
1. Clone the repository from [gitlab.fi.muni.cz](https://gitlab.fi.muni.cz/grp-edive/edive-platform) to your development machine.
2. Add Mirror library
   1. Download [Mirror 40.0.9](https://github.com/vis2k/Mirror/tree/v40.0.9)
   2. From downloaded .zip file, copy just: `Mirror-40.0.9/Assets/Mirror` folder and paste it into `...eDIVE project/Assets/_NonVersioned` folder
3. Open the eDIVE project in Unity editor. First opening could takes few minutes as Unity is parsing everything. Subsequent openings are significantly faster. 
4. Check the console tab. If there is high number of errors (>300), it probably means, that you did not import Mirror library correctly. Or you are using different version of Unity. Re-check previous steps.
5. Add some teaching module(s). Use top menu: **Edive → Project Setup** to clone some already released module(s). 
   1. If you open this menu for very first time, you will need to assign data holder object object. Just click on file selection icon (top right corner) and from offered files, select "PublicEdiveSetup". Once setted up, Unity should not ask you about this again. 
   2. Each module has its own GIT repository. This menu allows automatic cloning of most recent version of module. Most of modules also have additional data-pack containing e.g. videos, large terrain data, etc., so - for full functionality - download also related data-pack. 
   ![Screenshot](../resources/setup/Project_Setup.png)
6. At some point after opening the project (often, this happens after first hitting "play" button for first time), Unity will ask you to import TMP (TextMesh Pro) assets. eDIVE indeed uses TMP, therefore confirm import by clicking on **Import TMP Essentials ** button. Second part - ** TMP Examples and Extras ** is not necessary for eDIVE project.
![Screenshot](../resources/setup/TMP_Importer_Prompt.png)
7. Set default module:
   1. Open the `eDIVE_Main`Unity scene, located at `...\Assets\EdiveCore\Scenes` 
   2. In the `Edive_NetworkManager` game object, assign the starting scene of teaching module (the scene name usually start with `NetRoom` prefix) to the `online scene` field.
      ![Screenshot](../resources/setup/Online_Scene.png)
8. Add scene of freshly loaded modules to build settings
   ![Screenshot](../resources/setup/Build-settings.png)
9. At this point, you should be able to run project from editor and also to build the project into executable file. For automated building pipeline, there are buttons such as `Build VR Client` and `Automated Build of Desktop Server` in **eDIVE Settings** tab. See [Creating eDIVE builds](./building.md) section for more details.  

>[!NOTE]
>While downloading more modules could be beneficial for exploring eDIVE and its features, keep in mind, that size of modules varies from few MB to more than 1GB. Additional modules therefore implies not just higher disk space consumption, but also increased time of opening the project in Unity, creating new builds, etc.

## Voice Chat Setup
eDIVE platform uses 3rd party solution for audio communication. In current version, we are supporting only [VIVOX](https://unity.com/products/vivox). In future, we are planning to add support also for an open-source audio-chat solution.

### For developers from MUNI: 
You can use [this](https://drive.google.com/file/d/1lb7pFJdgkm1ZAHiBahKFt9s0LcqCvS0o/view?usp=drive_link) package containing all necessary files. 
1. If you do not have access yet, ask for it. For now, we are approving each request individually. 
2. Download and unzip the package.
3. Copy content of package into `...\Assets\_NonVersioned\` folder. 

### For developers outside of Masaryk University:
1. You will need [Unity Gaming Services](https://dashboard.unity3d.com) account.
2. Add Vivox package to your eDIVE project by following [instructions](https://dashboard.unity3d.com/vivox) provided by Unity.
3. Create a new `UVRN/VivoxSetup` asset and assign it too.

> [!IMPORTANT]
> Vivox provide a "free plan" and can be used without any fees within some limit of concurrent users.
> But, be sure to carefully read the licence, the user limits and especially about the fees applied, if the limit is exceeded.
