# eDIVE Platform

## About
eDIVE is a software platform for education in collaborative immersive virtual environment based on Unity game engine. The main purposes of eDIVE are:
1. Simplify the process of creating educational scenarios to developers.
2. Allow teachers and students to conduct education in custom-made virtual environments with tailored functionality.
3. Help researchers from various fields including computer science, education or psychology to conduct their research related to VR, online collaboration, education and more.

## README
Be sure to check our gitlab [Readme](https://gitlab.fi.muni.cz/grp-edive/edive-platform/-/blob/main/README.md) for basic info about project, licence, etc.

## Documentation
* [For end-users](manual/User_Manual.md) 
* [For developers](manual/Introduction.md)
