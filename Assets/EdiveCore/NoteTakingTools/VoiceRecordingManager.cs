using UnityEngine;
using UVRN.Player;
using UnityEngine.UI;
using TMPro;
using UVRN;

//opacut: this file is ported from old code. Most of the functionality is left intact, but 
//        I put an emphasis on untangling this class from the rest of the code in an attempt to make it more modular.
//        Currently the only interactions with this class from the outside should be by calling
//        ToggleVoiceRecording(controllerTransform) and passing the controller used.
//        This class then takes care of displaying the progress of the recording
//        and of saving the .wav file, before passing back control flow.
//        TODO: Is it possible to not need to pass the controllerTransform?
//
//        TO ACCESS THE RECORDINGS
//        unfortunately, currently you need to open the headset file system in the PC and find the folder
//          Android/data/cz.muni.edive.eDIVE/files/eDIVE/VoiceRecordings
//        we do realize this is a bit cumbersome, so the next step would definitely be to create a way of displaying
//        the recordings directly in the headset, and ultimately, in the eDIVE app itself.
namespace Edive.NoteTakingTools
{
    // Class responsible for creating voice recordings as a form of notes.
    // When the recording process starts, the player is muted.
    // When the process ends, the player is muted/unmuted - the same he was before recording.
    // The recording process is shown on the UI slider
    // Maximum time of one recording is 60 seconds. This can be adjusted by changing the "maxClipDurationSec"
    public class VoiceRecordingManager : MonoBehaviour
    {
        [SerializeField]
        private UVRN_NetworkManager m_NetworkManager;
        [SerializeField]
        private UVRN_NetworkManager NetworkManager
        {
            get
            {
                if (!m_NetworkManager)
                {
                    m_NetworkManager = (UVRN_NetworkManager)UVRN_NetworkManager.singleton;
                }
                if (m_NetworkManager != UVRN_NetworkManager.singleton)
                {
                    Debug.LogError("Net manager instance changed.");
                }
                return m_NetworkManager;
            }
        }

        // User Interface for the recording
        [SerializeField]
        private GameObject recordingDescriptions;

        [SerializeField]
        private GameObject recordingOnText;

        // slider showing the process of the recording
        [SerializeField]
        private Slider timerSlider;

        // text on the slider
        [SerializeField]
        private TextMeshProUGUI timerText;

        public float currTime;
        public float recordingStartTime;

        private bool recording = false;

        private Transform currControllerTransform;

        private AudioClip micRecording;


        private int frequency = 44100;
        public int maxClipDurationSec = 60;


        // how many times should the microphone audio buffer be larger that the reading window
        private const int microphoneBufferSize = 2;

        private UVRN_Player localPlayer;

        // If the player was muted before the recording, they will stay muted after
        private bool mutedPreviously = false;

        void Start()
        {
            // timer set-up
            timerSlider.maxValue = maxClipDurationSec;
            timerSlider.value = 0;
            timerText.text = string.Format("{0:0}:{1:00}", 0, 0);

            // microphone set-up
            if (Microphone.devices.Length == 0)
            {
                Debug.LogError("No microphone found!");
            }
            else
            {
                int minFreq, maxFreq;
                // Passing null to Microphone methods selects default device
                Microphone.GetDeviceCaps(null, out minFreq, out maxFreq);

                // If max/min frequency = 0, the device supports any frequency
                if (maxFreq > 0 && maxFreq < frequency)
                {
                    frequency = maxFreq;
                }
            }

        }

        void Update()
        {
            // move text to the current controller
            if (recording)
            {
                recordingDescriptions.transform.position = currControllerTransform.position;
                recordingDescriptions.transform.rotation = currControllerTransform.rotation;

                currTime = Time.time - recordingStartTime;
                int minutes = Mathf.FloorToInt(currTime / 60);
                int seconds = Mathf.FloorToInt(currTime - minutes * 60);

                timerText.text = string.Format("{0:0}:{1:00}", minutes, seconds);

                // the recording should be stopped with voiceRecording = false
                if (currTime >= maxClipDurationSec)
                    // the recording is stopped already 
                    timerSlider.value = maxClipDurationSec;
                else
                    timerSlider.value = currTime;

            }
        }

        public void ToggleVoiceRecording(Transform controllerTransform)
        {
            if (recording)
            {
                EndVoiceRecording();
            }
            else
            {
                StartVoiceRecording(controllerTransform);
            }
        }

        private void StartVoiceRecording(Transform controllerTransform)
        {
            Debug.Log("Starting voice recording.");
            if (!localPlayer)
            {
                LoadLocalPlayer();
            }
            recording = true;
            // change text to recording on
            recordingOnText.SetActive(true);
            recordingDescriptions.SetActive(true);

            Debug.Log("Starting microphone.");
            micRecording = Microphone.Start(null, true, maxClipDurationSec * microphoneBufferSize, frequency);

            mutedPreviously = NetworkManager.VoiceChatManager.IsMicMuted();
            NetworkManager.VoiceChatManager.SetMicMuted(true);
            

            // change the timer slider
            timerSlider.maxValue = maxClipDurationSec;
            timerSlider.value = 0;
            recordingStartTime = Time.time;
            timerText.text = string.Format("{0:0}:{1:00}", 0, 0);

            currControllerTransform = controllerTransform;
            Debug.Log("Voice recording started.");
        }

        private void LoadLocalPlayer()
        {
            localPlayer = UVRN_Player.LocalPlayer;
            if (!localPlayer)
            {
                Debug.LogError("Error finding local player for voice recording");
            }
        }

        private void EndVoiceRecording()
        {
            Debug.Log("Ending voice recording.");
            recordingOnText.SetActive(false);
            recordingDescriptions.SetActive(false);

            // end recording
            recording = false;
            Microphone.End(null);
            SaveVoiceRecording(micRecording);

            // unmute player unless they were muted before the recording
            NetworkManager.VoiceChatManager.SetMicMuted(mutedPreviously);
            Debug.Log("Voice recording ended.");
        }

        private void SaveVoiceRecording(AudioClip currRecording)
        {
            Debug.Log("Saving voice recording.");

            // Specific name of the recording - makes sure recording names are unique by adding the time they were created
            string name = "/VoiceRecording_" + System.DateTime.Now.ToString("yyyy-MM-dd_HH:mm:ss");

            SavWav.Save(name, currRecording);
        }
    }
}