using UnityEngine;
using UnityEngine.InputSystem;


//opacut: this is a very abridged version of ToolManager from the old NoteTakingTools code.
//        it serves as an impromptu interface between the VoiceRecordingManager and edive.
//        When time comes to refactor the old code, feel free to remove this class, or use anything usable.
namespace Edive.NoteTakingTools
{
    // Class representing the Manager controlling the note-taking tools
    // Takes care of picking, starting and ending the desired note-taking tool.
    public class ToolManager : MonoBehaviour
    {
        // action for activating object - trigger press
        // recommended setup - left and right trigger presses (activate)
        [SerializeField]
        private InputActionReference activateActionLeft;

        [SerializeField]
        private InputActionReference activateActionRight;

        void OnEnable()
        {
            activateActionLeft.action.started += ActivateActionLeft_performed;
            activateActionRight.action.started += ActivateActionRight_performed;
        }

        void OnDisable()
        {
            activateActionLeft.action.started -= ActivateActionLeft_performed;
            activateActionRight.action.started -= ActivateActionRight_performed;
        }

        private void ActivateActionLeft_performed(InputAction.CallbackContext ctx)
        {
            voiceRecordingManager.ToggleVoiceRecording(leftController.transform);
        }

        private void ActivateActionRight_performed(InputAction.CallbackContext ctx)
        {
            voiceRecordingManager.ToggleVoiceRecording(rightController.transform);
        }

        // References to the controllers and headset camera are only present in this class. 
        // This makes the bundle easier to import into a new project. 
        // Other classes, that need the references to the controllers or the headset get
        // them on their start.

        [SerializeField]
        private GameObject leftController;

        [SerializeField]
        private GameObject rightController;


        [SerializeField]
        private GameObject headsetCamera;


        [SerializeField]
        private VoiceRecordingManager voiceRecordingManager;


    }
}