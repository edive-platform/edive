// Author Vojtech Bruza
using System;
using System.Collections;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

namespace Edive.Networking.HTTP
{
    public class Edive_HTTPNetworkManager : MonoBehaviour
    {
        private const string CODE_CHARS = "AB0123456789";

        private Edive_NetworkManager m_netManager;
        private Edive_NetworkManager NetworkManager
        {
            get
            {
                if (!m_netManager)
                {
                    m_netManager = (Edive_NetworkManager)Edive_NetworkManager.singleton;
                }
                return m_netManager;
            }
        }

        public static readonly HttpClient client = new HttpClient();

        private IEnumerator serverRefreshCoroutine = null;
        private string serverSecret = null;
        private string registeredWithCode = null;
        private const float refreshTime = 10;

        protected void OnEnable()
        {
            if (NetworkManager.EdiveSettings.ServerSettings.AutoRegisterServerOnline) NetworkManager.Server_OnStart.AddListener(RegisterServerByCode);
            NetworkManager.Server_OnStop.AddListener(DisposeServer);
        }

        protected void OnDisable()
        {
            if (NetworkManager.EdiveSettings.ServerSettings.AutoRegisterServerOnline) NetworkManager.Server_OnStart.RemoveListener(RegisterServerByCode);
            NetworkManager.Server_OnStop.RemoveListener(DisposeServer);
        }

        private IEnumerator LogServerCode()
        {
            yield return new WaitForSecondsRealtime(5);

            Debug.Log($"The server code is {registeredWithCode}");
        }

        private void DisposeServer()
        {
            if (!string.IsNullOrEmpty(serverSecret))
            {
                DisposeServer(new ServerDisposeRequest { secret = serverSecret });
            }
        }

        private void RegisterServerByCode()
        {
            string prepickedCode = NetworkManager.EdiveSettings.ServerSettings.ServerCode;
            RegisterServerByCode(
                new ServerRegisterRequest
                {
                    code = prepickedCode,
                    org = "", // TODO
                    address = GetIp(),
                    port = NetworkManager.Port,
                    flavour = Application.productName,
                    version = Application.version,
                    time = $"{DateTime.Now:yyyy-MM-dd_HH:mm:ss}"
                },
                RegisterServerByCode_Callback);
        }

        private void RegisterServerByCode_Callback(ServerRegisterResponse.Data response)
        {
            Debug.Log($"Server registered with code {response.code}");

            registeredWithCode = response.code;
            serverSecret = response.secret;
            serverRefreshCoroutine = ServerRegistrationRefresh(new ServerRefreshRequest
                {
                    title = NetworkManager.EdiveSettings.ServerSettings.ServerTitle,
                    secret = serverSecret
                });
            StartCoroutine(serverRefreshCoroutine);

            StartCoroutine(LogServerCode());
        }

        private IEnumerator ServerRegistrationRefresh(ServerRefreshRequest request)
        {
            while (true)
            {
                // refresh immediately after register to update the name etc.
                ServerRefreshRequest(request);
                yield return new WaitForSecondsRealtime(refreshTime);
            }
        }

        private async void ServerRefreshRequest(ServerRefreshRequest req)
        {
            string serverManagerURL = GetServerManager();
            if (serverManagerURL == null) return;

            const string endpoint = "server/refresh";

            var response = await client.PostAsync(Path.Combine(serverManagerURL, endpoint), ToJsonContent(req));
            if (!response.IsSuccessStatusCode)
            {
                Debug.LogError($"Server refresh failed: '{response.StatusCode}'.");
                RegisterServerAgain();
                return;
            }
            var responseObject = await FromJson(response, new ServerRefreshResponse());

            if (responseObject.status != 0)
            {
                Debug.LogError($"Server refresh failed: '{responseObject.message}'. Trying to register server again.");
                RegisterServerAgain();
                return;
            }
        }

        private void RegisterServerAgain()
        {
            // if the coroutine was already running, stop it first (can happen when the server needs to register again)
            if (serverRefreshCoroutine != null) StopCoroutine(serverRefreshCoroutine);
            RegisterServerByCode();
        }

        private async void DisposeServer(ServerDisposeRequest req)
        {
            string serverManagerURL = GetServerManager();
            if (serverManagerURL == null) return;

            const string endpoint = "server/dispose";

            var response = await client.PostAsync(Path.Combine(serverManagerURL, endpoint), ToJsonContent(req));
            if (!response.IsSuccessStatusCode)
            {
                Debug.LogError($"Server dispose failed: '{response.StatusCode}'.");
                return;
            }
            var responseObject = await FromJson(response, new ServerDisposeResponse());

            if (responseObject.status != 0)
            {
                Debug.LogError($"Server dispose failed: '{responseObject.message}'. Will still be disposed after a minute or so.");
                return;
            }
        }

        private async void RegisterServerByCode(ServerRegisterRequest req, UnityAction<ServerRegisterResponse.Data> callback)
        {
            string serverManagerURL = GetServerManager();
            if (serverManagerURL == null) return;

            const string endpoint = "server/register";
            const string errorMessage = "Server code registration failed, but the server will still be accessible via its IP.";

            HttpResponseMessage response = null;
            try
            {
                response = await client.PostAsync(Path.Combine(serverManagerURL, endpoint), ToJsonContent(req));
            }
            catch (Exception e)
            {
                PrintErrorMsg(errorMessage, e.Message);
                return;
            }
            if (!response.IsSuccessStatusCode)
            {
                PrintErrorMsg(errorMessage, response.StatusCode.ToString());
                return;
            }
            var responseObject = await FromJson(response, new ServerRegisterResponse());

            if (responseObject.status != 0)
            {
                PrintErrorMsg(errorMessage, responseObject.message);
                return;
            }

            try
            {
                callback.Invoke(responseObject.data);
            }
            catch (Exception)
            {
                Debug.LogWarning("callback does not exist");
            }
        }

        private static void PrintErrorMsg(string errorMessage, string error)
        {
            Debug.LogError($"{errorMessage} Error '{error}'.");
        }

        // TODO move these to a specific class only for managing the codes?
        // -- also generalize the methods? (all have status, message and data)
        // ...but that would probably not possible right now since structs...so change them to classes?
        public async void GetServerByCode(string org, string code, UnityAction<QueryServerResponse.Data> callback)
        {
            if (string.IsNullOrEmpty(code))
            {
                Debug.LogError("No code provided.");
                return;
            }

            string serverManagerURL = GetServerManager();
            if (serverManagerURL == null) return;

            var url = Path.Combine(serverManagerURL, $"query/server?org={org}&code={code}");
            Debug.Log("Request url: " + url);
            var response = await client.GetAsync(url);
            if (!response.IsSuccessStatusCode)
            {
                Debug.LogError($"Client request failed for server from code failed: '{response.StatusCode}'.");
                return;
            }
            var responseObject = await FromJson(response, new QueryServerResponse());

            if (responseObject.status != 0)
            {
                Debug.LogError($"Client request for server from code failed: '{responseObject.message}'");
                // TODO some UI info
                return;
            }

            try
            {
                callback.Invoke(responseObject.data);
            }
            catch (Exception)
            {
                Debug.LogWarning("callback does not exist");
            }
        }

        private string GetServerManager()
        {
            var serverManagerURL = NetworkManager.EdiveSettings?.RuntimeSettings?.serverManagerUrl;
            if (string.IsNullOrWhiteSpace(serverManagerURL))
            {
                Debug.LogError("The app setup is missing (no server manager URL).");
                return null;
            }
            return serverManagerURL;
        }


        #region API helper functions

        public static StringContent ToJsonContent(object o)
        {
            return new StringContent(o == null ? "{}" : JsonUtility.ToJson(o), Encoding.UTF8, "application/json");
        }

        public static async Task<T> FromJson<T>(HttpResponseMessage response, T definition)
        {
            if (response == null) return definition;
            var responseString = await response.Content.ReadAsStringAsync();
            if (responseString == null) return definition;
            return JsonUtility.FromJson<T>(responseString);
        }

        private string GetIp()
        {
            return NetworkManager.EdiveSettings.ServerSettings.LocalIP ? GetLocalIp() : GetExternalIp();
        }

        public static string GetExternalIp()
        {
            return new WebClient()
                .DownloadString("http://ipv4.icanhazip.com")
                .Replace("\n", "")
                .Replace("\r", "")
                .Replace(" ", "");
        }

        public static string GetLocalIp()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList.Reverse()) // Need to take the last adapter
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new System.Exception("No network adapters with an IPv4 address in the system!");
        }

        #endregion

        private static string GetRandomCode(int lenght)
        {
            return new string(Enumerable.Repeat(CODE_CHARS, lenght).Select(s => s[UnityEngine.Random.Range(0, CODE_CHARS.Length)]).ToArray());
        }
    }
}
