﻿// Created by Jonas Rosecky, Vojtech Bruza
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Edive.Networking.HTTP.ServerList
{
    [Serializable]
    public struct ListRequest { }

    [Serializable]
    public class ListResponse
    {
        [SerializeField]
        public string result;

        [SerializeField]
        public string error;

        [SerializeField]
        public List<ServerInfo> servers = new List<ServerInfo>();
    }

    [Serializable]
    public struct RegisterRequest
    {
        public string address;
        public int port;
        public string title;
        public string version;
        public string secret;
        public bool dev;
    }

    [Serializable]
    public struct RegisterResponse
    {
        public string result;
        public string error;
        public string token;
    }

    [Serializable]
    public struct ServerInfo
    {
        [SerializeField]
        public string address;

        [SerializeField]
        public string port;

        [SerializeField]
        public string title;
        [SerializeField]
        public string version;
        [SerializeField]
        public string dev;

        [SerializeField]
        public bool IsDev { get => dev == "1"; }
    }

    [Serializable]
    public struct UpdateRequest
    {
        public string token;
    }

    [Serializable]
    public struct UpdateResponse
    {
        public string result;
        public string error;
    }

}
