// Created by Vojtech Bruza
using Edive.Interactions.Transformations;
using Edive.Tools;
using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UVRN;
using UVRN.Logging;

namespace Edive.Networking
{
    public class Edive_StickyNoteSpawner : NetworkBehaviour
    {
        [SerializeField]
        private StickyNotesManager stickyNotesManager;
        [SerializeField]
        private TransformManager stickyNotesTransformManager;

        [Tooltip("Position where the note will be spawned. Then lerps to its scene position.")]
        public Transform spawnPoint;
        public float spawnAnimationTime = 1;
        public AnimationCurve animationCurve = AnimationCurve.EaseInOut(0, 0, 1, 1);

        private List<int> notActiveNotes = new List<int>();

        [Tooltip("When false, will use the order from the StickyNotesManager. When true will shuffle the notes randomly.")]
        public bool spawnRandomOrder = false;

        #region Commands
        [Command(requiresAuthority = false)]
        public void Cmd_ResetAndShuffle()
        {
            ResetNotesPositions();
            SetActiveNotes(false);
            ShuffleNotes();
        }

        public void SpawnNote_CallCmd()
        {
            Cmd_SpawnRandomNote();
        }

        [Command(requiresAuthority = false)]
        public void Cmd_SpawnRandomNote(NetworkConnectionToClient sender = null)
        {
            if (notActiveNotes.Count == 0)
            {
                // there are no unspawned notes
                Rpc_NoNextNote(sender);
            }
            else SpawnRandomNote();
        }
        #endregion


        #region Server Methods

        public override void OnStartServer()
        {
            // Transform manager must be set to not save in start. Then the positions are saved here (because this can be called before Start)
            stickyNotesTransformManager.SaveTransforms();
            ResetNotesPositions();
            ShuffleNotes();
        }

        [Server]
        private void ShuffleNotes()
        {
            // reset not active notes list on the server
            notActiveNotes.Clear();
            for (int i = 0; i < stickyNotesManager.NotesCount; i++)
            {
                notActiveNotes.Add(i);
            }
            // shuffle on the server
            if (spawnRandomOrder)
            {
                notActiveNotes.Sort((a, b) => { return (UnityEngine.Random.value < 0.5) ? -1 : 1; });
                UVRN_Logger.Instance.LogEntry("Server", "Shuffling notes");
            }
        }

        [Server]
        private void SetActiveNotes(bool active)
        {
            // deactivate notes
            for (int i = 0; i < stickyNotesManager.NotesCount; i++)
            {
                SetActiveNote(i, active);
            }
        }

        [Server]
        private void SetActiveNote(int index, bool active)
        {
            stickyNotesManager.GetNote(index).GetComponent<UVRN_Toggle>().SetActive(active);
        }

        [Server]
        private void ResetNotesPositions()
        {
            for (int i = 0; i < stickyNotesManager.NotesCount; i++)
            {
                var noteTransform = stickyNotesManager.GetNote(i).transform;
                noteTransform.position = spawnPoint.position;
                noteTransform.rotation = spawnPoint.rotation;
            }
        }

        [Server]
        [ContextMenu("Spawn Random Note")]
        private void SpawnRandomNote()
        {
            if (notActiveNotes.Count > 0)
            {
                int i = notActiveNotes[0];
                notActiveNotes.RemoveAt(0);
                SetActiveNote(i, true);

                var note = stickyNotesManager.GetNote(i);

                // move note
                var targetTransformData = note.gameObject.GetComponent<AdditionalTransformData>();
                if (targetTransformData) StartCoroutine(MoveNote(note, spawnPoint, targetTransformData, spawnAnimationTime));

                UVRN_Logger.Instance.LogEntry("Server", "Spawned note " + note.GetText());
            }
        }

        [TargetRpc]
        private void Rpc_NoNextNote(NetworkConnection target)
        {
            Debug.Log("There are no next notes to spawn.");
            // TODO some better visual clue?
        }

        [Server]
        private IEnumerator MoveNote(StickyNoteController note, Transform spawnTransform, AdditionalTransformData targetTransform, float time)
        {
            float t = 0f;
            while (t < time)
            {
                yield return new WaitForEndOfFrame();
                float value = animationCurve.Evaluate(t / time);
                note.transform.position = Vector3.Lerp(spawnTransform.position, targetTransform.savedPosition, value);
                note.transform.rotation = Quaternion.Lerp(spawnTransform.rotation, targetTransform.savedRotation, value);
                // TODO use net transform scale
                //note.transform.localScale = Vector3.Lerp(spawnTransform.localScale, targetTransform.savedLocalScale, value);
                t += Time.deltaTime;
            }
        }
        #endregion
    }
}
