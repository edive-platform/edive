// Created by Vojtech Bruza
using Mirror;
using TMPro;

namespace Edive.Networking
{
    public class Edive_SyncText : NetworkBehaviour
    {
        private TMP_Text text;

        [SyncVar(hook = nameof(HandleTextChange))]
        private string sv_text;

        [Command(requiresAuthority = false)]
        public void Cmd_SetText(string text)
        {
            HandleTextChange(sv_text, text);
        }

        private void HandleTextChange(string _old, string _new)
        {
            sv_text = _new;
            text.text = sv_text;
        }
    }
}
