﻿// Created by Vojtech Bruza, Jonas Rosecky
using Edive.App.Settings;
using Edive.Networking.HTTP;
using Edive.Networking.HTTP.ServerList;
using Mirror;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UVRN;
using UVRN.Player;

namespace Edive.Networking
{
    [Serializable]
    public class UnityEventServerInfoList : UnityEvent<List<ServerInfo>> { }

    public class Edive_NetworkManager : UVRN_NetworkManager
    {
        // TODO move the http stuff to there:
        public Edive_HTTPNetworkManager httpNetManager = null;

        #region Serialized fields

        [Header("Edive")]

        public EdiveSettingsObject EdiveSettings;

        [Tooltip("Callback when client fetches info about online servers from manager")]
        public UnityEventServerInfoList Client_OnGetServers = new UnityEventServerInfoList();

        #endregion

        #region Private properties

        private string serverToken;
        [SerializeField]
        private bool showAvatarForDesktopPlayers = false;

        #endregion

        public override void OnStartServer()
        {
            base.OnStartServer();

            if (EdiveSettings.ServerSettings.EnableLogging)
            {
#if UNITY_EDITOR
                var logFolder = "_NonVersioned/Logs/";
#else
        var logFolder = "Logs";
#endif
                logger.Init(logFolder, $"{EdiveSettings.ServerSettings.ServerTitle}_log.txt");
            }

        }

        #region Unity runtime

        public override void Awake()
        {
            if (!EdiveSettings) Debug.LogError("You need to assign the Edive settings first");

#if UNITY_ANDROID && !UNITY_EDITOR
            if (EdiveSettings.BuildSettings.connectionType == ConnectionType.Server)
            {
                Debug.LogError("Wrong build settings - set to server, not client. Setting to client for Android");
                EdiveSettings.BuildSettings.connectionType = ConnectionType.Client;
            }
#endif

            base.Awake();
            if (EdiveSettings.BuildSettings.connectionType == ConnectionType.Server)
            {
                // Load server settings from file
                EdiveSettings.ServerSettings.LoadServerSettingsFromFile();

                // Load roles from file
                Edive_PlayerManager playerManager = PlayerManager as Edive_PlayerManager;
                if (playerManager)
                {
                    playerManager.LoadRoles(EdiveSettings.ServerSettings);
                }
                else
                {
                    Debug.LogWarning("No player manager assigned. Is this intentional?");
                }
            }

            if (httpNetManager == null)
            {
                httpNetManager = GetComponent<Edive_HTTPNetworkManager>();
            }
        }

        public override void Start()
        {
            connectionType = EdiveSettings.BuildSettings.connectionType;
            Port = EdiveSettings.ServerSettings.Port;
            base.Start();
        }

        protected void OnEnable()
        {
            //if (EdiveSettings.ServerSettings.AutoRegisterServerOnline) Server_OnStart.AddListener(RegisterServer);

            if (connectionType == ConnectionType.Client && EdiveSettings.ClientSettings.autoConnectClientOnServerFound)
            {
                //GetServerList(AutoConnectGetServerListCallback);
            }
        }

        protected void OnDisable()
        {
            //if (EdiveSettings.ServerSettings.AutoRegisterServerOnline) Server_OnStart.RemoveListener(RegisterServer);
        }

        #endregion

        #region Public API methods

        /// <summary>
        /// Register server in a PHP server.
        /// </summary>
        public async void RegisterServer()
        {
            if (EdiveSettings.RuntimeSettings == null)
            {
                Debug.LogError("The app setup is missing (no server manager URL). Could not register server.");
                return;
            }

            var response = await Edive_HTTPNetworkManager.client.PostAsync(
                EdiveSettings.RuntimeSettings.serverListingManagerUrl + "register",
                Edive_HTTPNetworkManager.ToJsonContent(new RegisterRequest
                {
                    address = EdiveSettings.ServerSettings.LocalIP ? Edive_HTTPNetworkManager.GetLocalIp() : Edive_HTTPNetworkManager.GetExternalIp(),
                    port = Port,
                    title = EdiveSettings.ServerSettings.ServerTitle + GetServerPostfix(),
                    version = Application.version,
                    secret = EdiveSettings.RuntimeSettings.serverListingManagerSecret,
                    dev = EdiveSettings.BuildSettings.isDev
                }));
            var responseObject = await Edive_HTTPNetworkManager.FromJson(response, new RegisterResponse());

            if (responseObject.result != "ok")
            {
                Debug.LogError($"Server registeration failed: '{responseObject.error}'");
                return;
            }

            serverToken = responseObject.token;
            Debug.Log($"Server registered with token {serverToken}");
            Console.WriteLine("========== Server started and registered ==========");
            InvokeRepeating(nameof(SendServerUpdate), EdiveSettings.ServerSettings.UpdateFrequency, EdiveSettings.ServerSettings.UpdateFrequency);
        }

        /// <summary>
        /// Disables server ping which basically unregisters the server.
        /// </summary>
        public void UnregisterServer()
        {
            CancelInvoke(nameof(SendServerUpdate));
        }

        public async void SendServerUpdate()
        {
            if (EdiveSettings.RuntimeSettings == null)
            {
                Debug.LogError("The app setup is missing (no server manager URL). Could not update server info.");
                return;
            }

            var response = await Edive_HTTPNetworkManager.client.PostAsync(
                EdiveSettings.RuntimeSettings.serverListingManagerUrl + "update",
                Edive_HTTPNetworkManager.ToJsonContent(new UpdateRequest { token = serverToken }));
            var responseObject = await Edive_HTTPNetworkManager.FromJson(response, new UpdateResponse());

            if (responseObject.result != "ok")
            {
                Debug.LogError($"Server update failed: '{responseObject.error}'");
                return;
            }
        }

        private string GetServerPostfix()
        {
            return "_" + Application.productName;
        }

        /// <summary>
        /// Request all available servers.
        /// </summary>
        public async void GetServerList(UnityAction<List<ServerInfo>> callback)
        {
            if (EdiveSettings.RuntimeSettings == null)
            {
                Debug.LogError("The app setup is missing (no server manager URL). Could not list servers.");
                return;
            }

            var response = await Edive_HTTPNetworkManager.client.PostAsync(EdiveSettings.RuntimeSettings.serverListingManagerUrl + "list", Edive_HTTPNetworkManager.ToJsonContent(new ListRequest { }));
            var responseObject = await Edive_HTTPNetworkManager.FromJson(response, new ListResponse());

            if (responseObject.result != "ok")
            {
                Debug.LogError($"Client request for server list failed: '{responseObject.error}'");
                return;
            }

            // if the server is not development and the product names does not match do not show the server. If the client is dev, show all servers.
            var filtered = responseObject.servers.Where((srv) => (!srv.IsDev && srv.title.EndsWith(GetServerPostfix())) || (EdiveSettings.BuildSettings.isDev));

            try
            {
                callback.Invoke(new List<ServerInfo>(filtered));
            }
            catch (Exception)
            {
                Debug.LogWarning("GetServerList callback does not exist anymore");
            }
        }

        #endregion

        #region Client methods

        public void JoinServer(string address, string port)
        {
            networkAddress = address;
            Port = ushort.Parse(port);
            StartClient();
        }

        private void AutoConnectGetServerListCallback(List<ServerInfo> servers)
        {
            var suitable = servers.Where((srv) => srv.version == Application.version);

            if (suitable.Count() == 0)
            {
                Debug.LogWarning($"No suitable server found for version '{Application.version}'");
                return;
            }

            var server = servers.First();

            networkAddress = server.address;
            Port = ushort.Parse(server.port);
            Debug.Log($"Autoconnecting to {server.address}:{server.port}");
            StartClient();
        }

        public override void OnStartClient()
        {
            // default visibility should be true, but do not show the player for desktop builds
            if (EdiveSettings.BuildSettings.targetPlatform == EdiveSettingsObject.TargetPlatform.Desktop) UVRN_PlayerProfileManager.LocalProfile.visibleAvatar = showAvatarForDesktopPlayers;
            base.OnStartClient();
        }

#endregion
    }
}