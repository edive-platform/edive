// Created by Vojtech Bruza
using System;
using System.Collections.Generic;
using System.IO;

namespace Edive.App.Settings
{
    public class FileSettings
    {
        private Dictionary<string, string> propertyValuePairs = new Dictionary<string, string>();

        private static readonly char keyValueSeparator = '=';
        private static readonly char commentChar = '#';

        public bool LoadFromFile(string filePath)
        {
            if (!File.Exists(filePath))
            {
                return false;
            }
            else
            {
                try
                {
                    using (StreamReader file = new StreamReader(filePath))
                    {
                        string line;
                        while ((line = file.ReadLine()) != null)
                        {
                            // skip comments
                            if (line.StartsWith(commentChar.ToString())) continue;
                            var propertyValuePair = line.Split(keyValueSeparator);
                            if (propertyValuePair.Length == 2)
                            {
                                var key = propertyValuePair[0];
                                var value = propertyValuePair[1];
                                if (!string.IsNullOrWhiteSpace(value) && !string.IsNullOrWhiteSpace(key))
                                {
                                    propertyValuePairs[key] = value;
                                }
                            }
                            // otherwise ignoring lines
                        }
                    }
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="propertyName"></param>
        /// <returns>null if property was not found in the file</returns>
        public string Get(string propertyName)
        {
            string value = null;
            propertyValuePairs.TryGetValue(propertyName, out value);
            return value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="propertiesKeyValues">Properties (key, value, tooltip)</param>
        /// <returns></returns>
        public bool CreateFileWithProperties(string filePath, string[][] propertiesKeyValues)
        {
            var dirPath = Path.GetDirectoryName(filePath);
            if (!Directory.Exists(dirPath)) Directory.CreateDirectory(dirPath);
            if (!File.Exists(filePath))
            {
                string[] lines = new string[2 * propertiesKeyValues.Length];
                int i = 0;
                foreach (string[] kvt in propertiesKeyValues)
                {
                    lines[i] = commentChar + " " + kvt[2];
                    i++;
                    lines[i] = kvt[0] + keyValueSeparator + kvt[1];
                    i++;
                }
                File.WriteAllLines(filePath, lines);
                return true;
            }
            return false;
        }

        public override string ToString()
        {
            string s = "";
            foreach (var kv in propertyValuePairs)
            {
                s += kv.Key + keyValueSeparator + kv.Value + Environment.NewLine;
            }
            return s;
        }
    }
}
