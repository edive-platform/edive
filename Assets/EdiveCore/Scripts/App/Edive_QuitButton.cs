// Created by Vojtech Bruza
using Edive.Networking;
using UnityEngine;

namespace Edive.App
{
    /// <summary>
    /// To be able to disconnect before quiting the app.
    /// </summary>
    public class Edive_QuitButton : MonoBehaviour
    {
        #region Net Manager
        private Edive_NetworkManager m_netManager;
        private Edive_NetworkManager netManager
        {
            get
            {
                if (!m_netManager)
                {
                    m_netManager = (Edive_NetworkManager)Edive_NetworkManager.singleton;
                }
                return m_netManager;
            }
        }
        #endregion
        public void QuitToMenu()
        {
            netManager.Disconnect();
        }

        public void QuitApp()
        {
            netManager.Disconnect();
            Application.Quit();
        }
    }
}
