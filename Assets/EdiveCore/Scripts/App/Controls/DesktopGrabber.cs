// Created by Vojtech Bruza
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;

namespace Edive.App.Controls
{
    public class DesktopGrabber : MonoBehaviour
    {
#if !UNITY_SERVER
        [SerializeField]
        private Camera cam;

        private XRGrabInteractable grabbedInteractable;

        [SerializeField]
        private float rayCastMaxDistance = 10;
        [SerializeField]
        private LayerMask moveLayerMask = -1;
        [SerializeField]
        private LayerMask grabLayerMask = -1;

        public UnityEvent OnGrab = new UnityEvent();
        public UnityEvent OnRelease = new UnityEvent();

        bool alignX = true;
        bool alignY = true;
        bool alignZ = true;

        private void InvokeSelectEntered(XRBaseInteractable interactable)
        {
            interactable.firstHoverEntered.Invoke(new HoverEnterEventArgs());
            interactable.hoverEntered.Invoke(new HoverEnterEventArgs());
            interactable.selectEntered.Invoke(new SelectEnterEventArgs());
        }

        private void InvokeSelectExited(XRBaseInteractable interactable)
        {
            interactable.selectExited.Invoke(new SelectExitEventArgs());
            interactable.hoverExited.Invoke(new HoverExitEventArgs());
            interactable.lastHoverExited.Invoke(new HoverExitEventArgs());
        }

        private void Update()
        {
            if (Mouse.current == null) return;

            if (Mouse.current.leftButton.wasPressedThisFrame)
            {
                if (grabbedInteractable)
                {
                    // ungrab
                    InvokeSelectExited(grabbedInteractable);
                    grabbedInteractable = null;
                    Cursor.visible = true;
                    OnRelease.Invoke();
                }
                else
                {
                    RaycastHit hit;
                    Ray ray = cam.ScreenPointToRay(Mouse.current.position.ReadValue()); // TODO how to get mouse position from events
                    // grab
                    if (Physics.Raycast(ray, out hit, rayCastMaxDistance, grabLayerMask))
                    {
                        Cursor.visible = false;
                        var target = hit.transform.gameObject.GetComponentInParent<XRGrabInteractable>();
                        if (target && target.enabled)
                        {
                            grabbedInteractable = target;
                            InvokeSelectEntered(grabbedInteractable);
                            OnGrab.Invoke();
                        }
                    }
                }
            }
            if (grabbedInteractable)
            {
                RaycastHit hit;
                Ray ray = cam.ScreenPointToRay(Mouse.current.position.ReadValue()); // TODO how to get mouse position from events
                // where to move
                if (Physics.Raycast(ray, out hit, rayCastMaxDistance, moveLayerMask))
                {
                    //// find the closest point for all colliders
                    //Vector3 closestPoint = grabbedInteractable.colliders[0].ClosestPoint(hit.point);
                    //float latestDistance = Vector3.Distance(closestPoint, hit.point);
                    //for (int i = 1; i < grabbedInteractable.colliders.Count; i++)
                    //{
                    //    var newPossibleClosestPoint = grabbedInteractable.colliders[i].ClosestPoint(hit.point);
                    //    var newDistance = Vector3.Distance(newPossibleClosestPoint, hit.point);
                    //    if (newDistance < latestDistance)
                    //    {
                    //        latestDistance = newDistance;
                    //        closestPoint = newPossibleClosestPoint;
                    //    }
                    //}
                    //// move the object so it snaps to the surface
                    //var offset = grabbedInteractable.transform.position - closestPoint;

                    // move the object so it snaps to the surface
                    var originalPos = grabbedInteractable.transform.position;
                    Vector3 targetPoint = new Vector3(alignX ? hit.point.x : originalPos.x, alignY ? hit.point.y : originalPos.y, alignZ ? hit.point.z : originalPos.z);
                    grabbedInteractable.transform.position = targetPoint;
                }
            }

        }
#endif
    }
}
