// Created by Vojtech Bruza
using Edive.App.Settings;
using System.Collections.Generic;
using UnityEngine;
using UVRN;
using UVRN.Helpers;
using UVRN.Player;

namespace Edive.App.Controls
{
    public class Edive_ControlsManager : Singleton<Edive_ControlsManager>
    {
        public EdiveSettingsObject EdiveSettings;

        [Tooltip("VR controls")]
        [SerializeField]
        private UVRN_XRManager uvrn_XRManager;

        [Tooltip("Desktop controls")]
        [SerializeField]
        private Edive_DesktopControls edive_DesktopControls;

        [SerializeField]
        private AudioListener audioListener;

        public UVRN_TrackeableControls ActiveControls { get; private set; }

        // TODOD maybe should be start since the components can be uninitialized yet
        public override void Awake()
        {
#if UNITY_ANDROID && !UNITY_EDITOR
            if (EdiveSettings.BuildSettings.targetPlatform == EdiveSettingsObject.TargetPlatform.Desktop)
            {
                Debug.LogError("Wrong build settings - set to desktop, not VR. Setting to VR for Android");
                EdiveSettings.BuildSettings.targetPlatform = EdiveSettingsObject.TargetPlatform.VR;
            }
#endif
            base.Awake();
            switch (EdiveSettings.BuildSettings.targetPlatform)
            {
                // TODO smarter way to distinguish between VR and desktop (also get rid of the network manager reference here
                case EdiveSettingsObject.TargetPlatform.Desktop:
                    ActivateControls(edive_DesktopControls, new List<UVRN_TrackeableControls> { uvrn_XRManager });
                    break;
                case EdiveSettingsObject.TargetPlatform.VR:
                    ActivateControls(uvrn_XRManager, new List<UVRN_TrackeableControls> { edive_DesktopControls });
                    break;
                default:
                    break;
            }
        }

        private void ActivateControls(UVRN_TrackeableControls activateControls, IEnumerable<UVRN_TrackeableControls> destroyControls)
        {
            foreach (var dest in destroyControls) Destroy(dest.gameObject);

            activateControls.gameObject.SetActive(true);
            ActiveControls = activateControls;
            if (audioListener == null) Debug.LogError("No audio listener set for " + gameObject.name);
            activateControls.SetAudioListener(audioListener);
        }
    }
}
