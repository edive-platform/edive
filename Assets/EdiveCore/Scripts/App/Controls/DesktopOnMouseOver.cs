// Created by Vojtech Bruza
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Edive.App.Controls
{
    public class DesktopOnMouseOver : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        public UnityEvent OnMouseEnter = new UnityEvent();
        public UnityEvent OnMouseExit = new UnityEvent();

        public void OnPointerEnter(PointerEventData eventData)
        {
            OnMouseEnter.Invoke();
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            OnMouseExit.Invoke();
        }
    }
}