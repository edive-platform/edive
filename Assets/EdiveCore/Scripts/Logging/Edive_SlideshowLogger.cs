using UnityEngine;
using Mirror;
using Edive.Networking;
using UVRN.Logging;

namespace Edive.Logging
{
    [RequireComponent(typeof(SlideShowNetworking))]
    public class Edive_SlideshowLogger : UVRN_GenericLogger
    {
        private SlideShowNetworking slideshow;

        public override void Init()
        {
            base.Init(null, "Slideshow", true);
            slideshow = GetComponent<SlideShowNetworking>();
            slideshow.setSlideEvent.AddListener(LogSlideChange);
        }

        private void LogSlideChange(NetworkConnection conn, int slideNumber)
        {
            LogCustomMessage(GetNameFromConnection(conn), "changed slide to number", slideNumber.ToString());
        }
    }
}