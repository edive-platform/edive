﻿// Created by Vojtech Bruza
using UnityEngine;
using UnityEngine.Events;

namespace Edive.Utils
{
    // TODO remove this from build
    public class GenericEditorButton : MonoBehaviour
    {
        public UnityEvent onEditorButtonPress = new UnityEvent();

        public void Invoke()
        {
            onEditorButtonPress?.Invoke();
        }
    }
}
