// Created by Vojtech Bruza
using UnityEngine;

namespace Edive.Utils
{
    public static class Utilities
    {
        public static Vector3 RandomVec3(Vector3 min, Vector3 max)
        {
            return new Vector3(Random.Range(min.x, max.x), Random.Range(min.y, max.y), Random.Range(min.z, max.z));
        }

        public static void CopyTo(this Transform from, Transform to)
        {
            to.position = from.position;
            to.rotation = from.rotation;
            to.localScale = from.localScale;
        }
    }
}
