// Created by Jiri Chmelik, Vojtech Bruza
using System.Collections.Generic;
using UnityEngine;


namespace Edive.UI
{
    public class SlideshowController : MonoBehaviour
    {

        [SerializeField]
        private List<GameObject> slides;

        public int SlideCount => slides.Count;

        [SerializeField]
        private bool showFirstSlide = false;

        public int activeSlideNumber { get; private set; }

        [SerializeField]
        private GameObject nextButtonText;

        [SerializeField]
        private GameObject nextButtonBlankText;

        [SerializeField]
        private GameObject prevButtonText;

        [SerializeField]
        private GameObject prevButtonBlankText;

        void Start()
        {
            activeSlideNumber = -1;

            //set all slides hidden
            foreach (var slide in slides)
            {
                slide.SetActive(false);
            }

            if (showFirstSlide)
            {
                slides[0].SetActive(true);
                activeSlideNumber = 0;
            }
        }

        public void SetSlide(int slideNumber)
        {
            // dumb check
            // -1 is "empty slide"
            if (slideNumber < -1 || slideNumber > slides.Count)
            {
                Debug.LogError("Wrong number of slide: " + slideNumber);
                Debug.LogError("Only: " + slides.Count + " is available.");
                return;
            }

            //print("setting slide #" + slideNumber);

            //hide active slide, if any
            if (activeSlideNumber >= 0 && activeSlideNumber < slides.Count)
            {
                slides[activeSlideNumber].SetActive(false);
            }

            SetButtons(activeSlideNumber, slideNumber);

            //set, show selected slide
            activeSlideNumber = slideNumber;

            if (activeSlideNumber >= 0 && activeSlideNumber < slides.Count)
            {
                slides[activeSlideNumber].SetActive(true);
            }
        }

        // no time to make it nicer :D
        private void SetButtons(int currentSlideNumber, int targetSlideNumber)
        {
            // first blank slide
            if (currentSlideNumber == -1)
            {
                if (prevButtonBlankText) prevButtonBlankText.SetActive(true);
                if (prevButtonText) prevButtonText.SetActive(false);
            }
            // last blank slide
            else if (currentSlideNumber == slides.Count)
            {
                if (nextButtonBlankText) nextButtonBlankText.SetActive(true);
                if (nextButtonText) nextButtonText.SetActive(false);
            }

            // going from first blank to first slide
            if (targetSlideNumber == 0 && currentSlideNumber == -1)
            {
                if (prevButtonBlankText) prevButtonBlankText.SetActive(false);
                if (prevButtonText) prevButtonText.SetActive(true);
            }
            // going from last blank to last slide
            else if (targetSlideNumber == slides.Count - 1 && currentSlideNumber == slides.Count)
            {
                if (nextButtonBlankText) nextButtonBlankText.SetActive(false);
                if (nextButtonText)  nextButtonText.SetActive(true);
            }
        }

        public void NextSlide()
        {
            if (activeSlideNumber <= slides.Count - 1)
            {
                SetSlide(activeSlideNumber + 1);
            }

        }

        public void PrevSlide()
        {
            if (activeSlideNumber > 0)
            {
                SetSlide(activeSlideNumber - 1);
            }

            else if (activeSlideNumber == 0)
            {
                SetSlide(-1);
            }
        }



    }
}
