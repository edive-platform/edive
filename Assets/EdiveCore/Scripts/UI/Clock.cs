// Created by Vojtech Bruza
using System;
using TMPro;
using UnityEngine;

namespace Edive.UI
{
    public class Clock : MonoBehaviour
    {
        [SerializeField]
        private TMP_Text text;

        public bool format24h = true;

        public bool showDate = true;
        public bool showTime = true;
        public bool showSeconds = true;

        private void Update()
        {
            var date = showDate ? $"yyyy-MM-dd" : "";

            var space = showTime && showDate ? " " : "";

            var h = format24h ? "HH" : "hh";
            var s = showSeconds ? ":ss" : "";
            var time = showTime ? $"{h}:mm{s}" : "";

            text.text = DateTime.Now.ToString($"{date}{space}{time}");
        }
    }
}
