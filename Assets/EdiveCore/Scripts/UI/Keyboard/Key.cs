﻿// Created by Vojtech Bruza
using UnityEngine;
using UnityEngine.Events;

namespace Edive.UI.Keyboard
{
    public class Key : MonoBehaviour
    {
        [Tooltip("Should it send the text from the text mesh?")]
        public bool AddTextOnClicked = true;

        /// <summary>
        /// Maybe should default to false? Depends on the input field behaviour and Enter key behaviour
        /// </summary>
        public bool InvokeKeyboardSendOnClick = true;

        [SerializeField]
        private TextMesh t;

        public Keyboard keyboard;

        public UnityEvent Clicked = new UnityEvent();

        public string Text
        {
            get
            {
                return t.text;
            }
            set
            {
                t.text = value;
            }
        }

        private void Awake()
        {
            if (!t) t = GetComponentInChildren<TextMesh>();
            if (!keyboard) keyboard = GetComponentInParent<Keyboard>();
        }

        private void OnEnable()
        {
            Clicked.AddListener(OnClicked);
        }

        private void OnDisable()
        {
            Clicked.RemoveListener(OnClicked);
        }

        private void OnClicked()
        {
            if (AddTextOnClicked) keyboard.AddText(Text);
            if (InvokeKeyboardSendOnClick) keyboard.Send();
        }

        public void Click()
        {
            Clicked.Invoke();
        }
    }
}
