﻿// Created by Vojtech Bruza
using System;
using UnityEngine;
using UnityEngine.Events;

namespace Edive.UI.Keyboard
{
    public class Keyboard : MonoBehaviour
    {
        [Serializable]
        public class UnityEventString : UnityEvent<string> { }
        public UnityEventString OnSend = new UnityEventString();

        public bool AutoCapitalize = false;

        public string Text = "";

        private void OnDisable()
        {
            Text = "";
        }

        public void SetText(string text)
        {
            Text = text;
            if (AutoCapitalize) Text = CapitalizeWords(Text.ToLower());
        }

        public void AddText(string text)
        {
            SetText(Text + text);
        }

        public void RemoveLetter()
        {
            if (Text.Length > 0)
            {
                Text = Text.Substring(0, Text.Length - 1);
            }
        }

        public void Send()
        {
            OnSend.Invoke(Text);
        }

        #region Utils
        private string CapitalizeWords(string str)
        {
            string text = "";
            string[] words = str.Split(' ');

            if (words.Length > 1)
            {
                text += Capitalize(words[0]);
                for (int i = 1; i < words.Length; i++)
                {
                    text += " " + Capitalize(words[i]);
                }
                if (str.EndsWith(" ")) text += " ";
                return text;
            }
            // else return original
            return Capitalize(str);
        }

        private string Capitalize(string word)
        {
            if (word.Length == 0) return "";
            if (word.Length == 1) return char.ToUpper(word[0]).ToString();
            return char.ToUpper(word[0]) + word.Substring(1);
        }
        #endregion
    }
}
