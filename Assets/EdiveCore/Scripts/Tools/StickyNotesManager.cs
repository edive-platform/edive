// Created by Vojtech Bruza
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/// <summary>
/// To be run in editor mode
/// Instantiates a group of sticky notes objects from prefab, set texts, set positions
/// removes all old notes
/// </summary>
namespace Edive.Tools
{
    public class StickyNotesManager : MonoBehaviour
    {
        [SerializeField]
        private string[] texts;

        [SerializeField]
        private GameObject stickyNotePrefab;

        [SerializeField]
        private Vector3 positionStart;

        [SerializeField]
        private Vector3 positioningOffset = new Vector3(0.5f, 0, 0);

        [SerializeField]
        private Vector3 rowOffset = new Vector3(0, 0.5f, 0);

        [SerializeField]
        private int rows = 1;

        // list of existing notes - can be used to "throw" random note into scene...
        [SerializeField]
        private List<StickyNoteController> stickyNotes = new List<StickyNoteController>();

        public int NotesCount => stickyNotes.Count;

        [ContextMenu("Generate Sticky Notes")]
        void GenerateNotes()
        {
            DestroyNotes();

            GameObject stickyNote;
            Quaternion rotation = Quaternion.identity;
            //float halflength = texts.Length / 2.0f;
            //Vector3 position = this.transform.position - new Vector3(halflength * positioningOffset.x, halflength * positioningOffset.y, halflength * positioningOffset.z);
            Vector3 rowStartPosition = positionStart;

            //instantiate and setup new notes
            int count = Mathf.CeilToInt(texts.Length / (float)rows);
            Debug.Log(count);
            for (int r = 0; r < rows; r++)
            {
                var rowStart = r * count;
                Vector3 position = rowStartPosition;
                for (int i = rowStart; i < (rowStart + count) && i < texts.Length; i++)
                {
                    string text = texts[i];
#if UNITY_EDITOR
                    stickyNote = PrefabUtility.InstantiatePrefab(stickyNotePrefab, transform) as GameObject;
                    stickyNote.transform.position = position;
                    stickyNote.transform.rotation = rotation;
#else
                stickyNote = Instantiate(stickyNotePrefab, position, rotation, this.transform);
#endif
                    position += positioningOffset;

                    var stickyNoteController = stickyNote.GetComponent<StickyNoteController>();
                    stickyNoteController.SetText(text);

                    stickyNotes.Add(stickyNoteController);
                }
                rowStartPosition += rowOffset;
            }
        }

        [ContextMenu("Find Sticky Note Children")]
        private void FindChildren()
        {
            DestroyNotes();
            foreach (Transform t in transform)
            {
                stickyNotes.Add(t.gameObject.GetComponent<StickyNoteController>());
            }
        }

        public void DestroyNotes()
        {
            foreach (StickyNoteController note in stickyNotes)
            {
#if UNITY_EDITOR
                if (note) DestroyImmediate(note.gameObject);
#else
            if (note) Destroy(note.gameObject);
#endif
            }
            stickyNotes.Clear();
        }

        public StickyNoteController GetNote(int index)
        {
            return stickyNotes[index];
        }
    }
}
