// Created by Vojtech Bruza
using UnityEngine;

namespace Edive.Tools
{
    public class StickyNoteController : MonoBehaviour
    {
        [SerializeField]
        private GameObject model;

        [SerializeField]
        private string text;

        [SerializeField]
        private TextMesh textFront;

        [SerializeField]
        private TextMesh textBack;


        // Start is called before the first frame update
        void Start()
        {
            SetText(text);
            if (!model) model = this.gameObject;
        }

        public void SetText(string text)
        {
            this.text = text;
            if (textFront != null && textBack != null)
            {
                textFront.text = text;
                textBack.text = text;
            }
            else
            {
                Debug.LogError("Sticky note object missing reference to text object");
            }
        }

        public string GetText()
        {
            return text;
        }

        public void SetActive(bool active)
        {
            model.SetActive(active);
        }

        [ContextMenu("Toggle model")]
        public void Toggle()
        {
            model.SetActive(!model.activeSelf);
        }
    }
}
