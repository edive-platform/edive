// Created by Vojtech Bruza
using UnityEngine;

namespace Edive.Lobby
{
    public class InitUI : MonoBehaviour
    {
        [Tooltip("Will activate the first item on start and deactivate rest.")]
        [SerializeField]
        private GameObject[] group = null;

        private void OnEnable()
        {
            if (group == null || group.Length > 1)
            {
                group[0].SetActive(true);
                for (int i = 1; i < group.Length; i++)
                {
                    group[i].SetActive(false);
                }
            }
            else
            {
                Debug.LogWarning("No need to use this for less then 2 elements?");
            }
        }
    }
}
