﻿// Created by Vojtech Bruza
using Edive.Networking.HTTP.ServerList;
using UnityEngine;

namespace Edive.Lobby
{
    public class ServerListButton : MonoBehaviour
    {
        private ServerListRenderer serverList;

        public TextMesh TitleText;
        public TextMesh IPText;
        public TextMesh VersionText;

        public GameObject StatusIcon;

        public Material OKMaterial;
        public Material ErrorMaterial;

        private ServerInfo serverInfo;

        public void SetData(ServerInfo info, ServerListRenderer serverList)
        {
            serverInfo = info;
            this.serverList = serverList;

            TitleText.text = info.title;
            IPText.text = $"{info.address}:{info.port}";
            if (info.IsDev)
            {
                IPText.text += " (development)";
            }
            VersionText.text = info.version;

            if (info.version.Split('f')[0] == Application.version.Split('f')[0])
            {
                StatusIcon.GetComponent<MeshRenderer>().material = OKMaterial;
            }
            else
            {
                StatusIcon.GetComponent<MeshRenderer>().material = ErrorMaterial;
            }
        }

        public void OnClick()
        {
            serverList.Clicked(serverInfo.address, serverInfo.port);
        }
    }
}
