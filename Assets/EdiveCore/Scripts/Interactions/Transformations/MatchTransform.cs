﻿// Created by Vojtech Bruza
using UnityEngine;

namespace Edive.Interactions.Transformations
{
    public class MatchTransform : MonoBehaviour
    {
        [Tooltip("Set to this gameobject's transform if null.")]
        public Transform transformToMatch;
        [Tooltip("The transform will be batched with this one (pos, rot, scale).")]
        public Transform transformToMatchTo;

        public bool matchPosition = true;
        public bool matchRotation = true;
        public bool matchScale = false;

        private void Awake()
        {
            if (!transformToMatch) transformToMatch = transform;
        }

        public void Match()
        {
            if (matchPosition) transformToMatch.position = transformToMatchTo.position;
            if (matchRotation) transformToMatch.rotation = transformToMatchTo.rotation;
            if (matchScale) transformToMatch.localScale = transformToMatchTo.localScale;
        }
    }
}
