using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace Edive.Interactions.Transformations
{
    [RequireComponent(typeof(XRSimpleInteractable))]
    public class RotateInteractable : MonoBehaviour
    {
        private XRSimpleInteractable interactable = null;
        private XRRayInteractor _interactor = null;

        [SerializeField]
        private float sensitivity = 500;

        /// <summary>
        /// Position of the interactor at the time of grabbing it.
        /// </summary>
        private Vector3 _originalInteractorPosition;
        /// <summary>
        /// Rotation of the interactor at the time of grabbing it.
        /// </summary>
        private Quaternion _originalInteractorRotation;
        /// <summary>
        /// Rotation of this object at the time of grabbing it.
        /// </summary>
        private Quaternion _originalRotation;

        private Vector3 _smoothRotationVelocity;

        [Tooltip("Disable interaction when aiming outside of the object? (Consider increasing the sensitivity when true)")]
        [SerializeField]
        private bool disableOutsideBoundsInteraction = false;

        private void Awake()
        {
            interactable = GetComponent<XRSimpleInteractable>();
        }

        private void OnEnable()
        {
            interactable.selectEntered.AddListener(OnSelect);
            interactable.selectExited.AddListener(OnRlease);
        }

        private void OnDisable()
        {
            interactable.selectEntered.RemoveListener(OnSelect);
        }

        private void OnSelect(SelectEnterEventArgs args)
        {
            // get current interactor
            _interactor = args.interactor as XRRayInteractor;

            // save original rotations and positions (at the time of grab)
            _originalRotation = transform.rotation;
            _originalInteractorPosition = _interactor.transform.position;
            _originalInteractorRotation = _interactor.transform.rotation;

            _smoothRotationVelocity = Vector3.zero;
        }

        private void OnRlease(SelectExitEventArgs args)
        {
            _interactor = null;
            _smoothRotationVelocity = Vector3.zero;
        }

        private void FixedUpdate()
        {
            if (_interactor == null) return;

            // should interact when aiming outside the object?
            if (disableOutsideBoundsInteraction && !_interactor.TryGetCurrent3DRaycastHit(out _)) return;

            var eyeDirection = _originalInteractorPosition - transform.position;
            var eyeRightDirection = Vector3.Cross(Vector3.up, eyeDirection).normalized;
            var eyeUpDirection = Vector3.Cross(eyeDirection, eyeRightDirection).normalized;
                
            var moveDirection = transform.position - _interactor.transform.position;
                
            var x = Vector3.Dot(moveDirection, eyeRightDirection) * sensitivity;
            var y = Vector3.Dot(moveDirection, eyeUpDirection) * sensitivity;

            var xRotation = Quaternion.AngleAxis(-x, eyeUpDirection);
            var yRotation = Quaternion.AngleAxis(y, eyeRightDirection);

            // get rotation around the Z axis (twisting)
            var twistRotationValue = _interactor.transform.rotation.eulerAngles.z - _originalInteractorRotation.eulerAngles.z;
            var zRotation = Quaternion.AngleAxis(twistRotationValue, _interactor.transform.forward);

            // the final rotation for all axes
            var finalRotation = xRotation * yRotation * zRotation * _originalRotation;

            // set the rotation (smoothed)
            transform.rotation = SmoothDampQuaternion(transform.rotation, finalRotation, ref _smoothRotationVelocity, 0.1f);
        }

        public static Quaternion SmoothDampQuaternion(Quaternion current, Quaternion target, ref Vector3 currentVelocity, float smoothTime)
        {
            var c = current.eulerAngles;
            var t = target.eulerAngles;
            return Quaternion.Euler(
                Mathf.SmoothDampAngle(c.x, t.x, ref currentVelocity.x, smoothTime),
                Mathf.SmoothDampAngle(c.y, t.y, ref currentVelocity.y, smoothTime),
                Mathf.SmoothDampAngle(c.z, t.z, ref currentVelocity.z, smoothTime)
            );
        }
    }
}
