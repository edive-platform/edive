// Created by Vojtech Bruza
using System.Collections.Generic;
using UnityEngine;

namespace Edive.Interactions.Transformations
{
    public class TransformManager : MonoBehaviour
    {
        public bool AutoSaveOnStart = true;

        public List<AdditionalTransformData> transformsData = new List<AdditionalTransformData>();

        [SerializeField]
        private bool appplyPosition = true;

        [SerializeField]
        private bool applyRotation;

        [SerializeField]
        private bool applyScale;

        [SerializeField]
        private Vector3 translation;

        private bool initialPosition = true;

        private void Start()
        {
            if (AutoSaveOnStart) SaveTransforms();
        }

        public void SaveTransforms()
        {
            foreach (AdditionalTransformData d in transformsData)
            {
                d.SaveTransform();
            }
        }

        public void ApplyTransforms()
        {
            foreach (AdditionalTransformData t in transformsData)
            {
                t?.ApplySavedTransforms(appplyPosition, applyRotation, applyScale);
            }
        }

        // TODO create new class for QoL specific functionality
        /// <summary>
        /// move all cubes there and back
        /// </summary>
        public void TransformMetricCubesPositions()
        {
            Debug.Log("Moving Cubes");

            foreach (AdditionalTransformData t in transformsData)
            {
                //TODO DO NOT USE null propagation in Unity!
                t?.MoveMetricCube(initialPosition ? translation : -1.0f * translation);
            }
            initialPosition = !initialPosition;
        }

        [ContextMenu("Fill Children")]
        public void FillChildren()
        {
            foreach (Transform child in transform)
            {
                transformsData.Add(GetOrCreateTransformData(child));
            }
        }

        private AdditionalTransformData GetOrCreateTransformData(Transform t)
        {
            var data = t.gameObject.GetComponent<AdditionalTransformData>();
            if (!data) data = t.gameObject.AddComponent<AdditionalTransformData>();
            return data;
        }
    }
}
