// Created by Vojtech Bruza
using UnityEngine;

namespace Edive.Interactions
{
    public class ScaleTransform : MonoBehaviour
    {
        public enum ScaleState
        {
            Minimized,
            Normal,
            Maximized
        }

        private Vector3 normalScale = Vector3.one;
        public float MinimizedScale = 0.1f;
        public float MaximizedScale = 2f;

        private ScaleState m_currentState = ScaleState.Normal;
        public ScaleState CurrentState
        {
            get => m_currentState;
            set
            {
                switch (value)
                {
                    case ScaleState.Normal:
                        Normal();
                        break;
                    case ScaleState.Minimized:
                        Minimize();
                        break;
                    case ScaleState.Maximized:
                        Maximize();
                        break;
                    default:
                        break;
                }
            }
        }

        private void Awake()
        {
            normalScale = transform.localScale;
        }

        public void Normal()
        {
            transform.localScale = normalScale;
            m_currentState = ScaleState.Normal;
        }

        public void Minimize()
        {
            transform.localScale = MinimizedScale * normalScale;
            m_currentState = ScaleState.Minimized;
        }

        public void Maximize()
        {
            transform.localScale = MaximizedScale * normalScale;
            m_currentState = ScaleState.Maximized;
        }
    }
}
