﻿// Created by Vojtech Bruza
using Edive.Utils;
using UnityEditor;
using UnityEngine;

namespace EdiveEditor
{
    [CustomEditor(typeof(XRInteractibleEditorButton))]
    [CanEditMultipleObjects]
    public class XRInteractibleEditorButtonEditor : Editor
    {
        XRInteractibleEditorButton myScript;
        private void OnEnable()
        {
            myScript = (XRInteractibleEditorButton)target;
        }

        public override void OnInspectorGUI()
        {
            GUILayout.Space(30);
            if (GUILayout.Button("Simulate Button Press", new GUILayoutOption[] { GUILayout.Height(30) }))
            {
                myScript.Invoke();
            }
            GUILayout.Space(30);
        }
    }
}