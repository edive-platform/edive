// Created by Jonas Rosecky, Vojtech Bruza
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using UnityEditor;
using UnityEditor.PackageManager;
using UnityEngine;
using UnityEngine.XR;
using UVRN;

namespace EdiveEditor
{
    public static class Build
    {
        public const string BuildPathOption = "-customBuildPath";
        public const string BuildOptionsOption = "-customBuildOptions";
        public const string BuildTargetOption = "-buildTarget";
        //public const string ScriptingBackendOption = "-customScriptingBackend";

        private static string buildFolder => $"Builds/AutomatedBuilds";
        public static string BuildPath => $"{buildFolder}/[appType]/{Application.productName.Replace(" ", "") }_{Application.version}/{Application.productName.Replace(' ', '_')}.[extension]";

        #region Build script wrappers

        public static void BuildAllWrapper(BuildTarget serverTarget)
        {
            if (!BuildStartDialog()) return;

            BuildFinishedDialog(BuildAll(serverTarget), buildFolder);
        }
        public static void BuildServerWrapper(BuildTarget target)
        {
            if (!BuildStartDialog()) return;
            switch (target)
            {
                case BuildTarget.StandaloneWindows:
                case BuildTarget.StandaloneWindows64:
                    {
                        var buildPath = BuildPath
                            .Replace("[appType]", "Server")
                            .Replace("[extension]", "exe");
                        BuildFinishedDialog(BuildServer(target, buildPath), buildFolder);
                        break;
                    }
                case BuildTarget.StandaloneLinux64:
                    {
                        var buildPath = BuildPath
                            .Replace("[appType]", "Server (Linux)")
                            .Replace("[extension]", "x64");
                        BuildFinishedDialog(BuildServer(target, buildPath), buildFolder);
                        break;
                    }
            }
        }

        public static void BuildClientWrapper()
        {
            if (!BuildStartDialog()) return;

            BuildFinishedDialog(BuildClient(), buildFolder);
        }

        #endregion

        #region Build scripts

        public static bool BuildAll(BuildTarget serverTarget)
        {
            return BuildServer(serverTarget) && BuildClient();
        }

        public static bool BuildAll(BuildTarget serverTarget, string buildPathServer, string buildPathClient)
        {
            return BuildServer(serverTarget, buildPathServer) && BuildClient(buildPathClient);
        }

        public static bool BuildServer(BuildTarget target)
        {
            var buildPath = BuildPath
                .Replace("[appType]", "Server")
                .Replace("[extension]", "exe");

            return BuildServer(target, buildPath);
        }

        public static bool BuildServer(BuildTarget target, string buildPath)
        {
            // == Set connection type ==
            SetConnectionType(UVRN_NetworkManager.ConnectionType.Server);

            // == Disable XR ==
            XRSettings.enabled = false;
            foreach (var package in EditorUtils.EdiveSettings.BuildSettings.clientOnlyPackages)
            {
                WaitForCompletion(Client.Remove(package.Split('@')[0]));
            }
            WaitForUnityIdle();

            // == Setup build ==
            Log($"Build path set to '{buildPath}'.");

            // exclude scenes with "ClientOnly" string in title
            var scenes = EditorBuildSettings.scenes
                .Where(s => s.enabled && !string.IsNullOrWhiteSpace(s.path) && !s.path.Contains("ClientOnly"))
                .Select(s => s.path)
                .ToArray();
            Log($"Building the '{string.Join(", ", scenes)}' scenes.");

            var buildOptions = BuildOptions.EnableHeadlessMode;

            Log($"Switching build target");
            if (SwitchBuildTarget(target))
            {
                Log($"Target switched");
            }
            else
            {
                Log($"Error while switching target");
                return false;
            }

            // == Run build ==
            var report = BuildPipeline.BuildPlayer(new BuildPlayerOptions
            {
                locationPathName = buildPath,
                scenes = scenes,
                target = EditorUserBuildSettings.activeBuildTarget,
                subtarget = (int)StandaloneBuildSubtarget.Server,
                targetGroup = BuildPipeline.GetBuildTargetGroup(EditorUserBuildSettings.activeBuildTarget),
                options = buildOptions,
            });

            if (report.summary.result != UnityEditor.Build.Reporting.BuildResult.Succeeded)
            {
                Log($"Build failed - '{report.summary.result}'.");
            }
            else
            {
                Log("Build succeeded: " + report.summary.totalSize + " bytes");
            }


            // == Enable XR ==
            foreach (var package in EditorUtils.EdiveSettings.BuildSettings.clientOnlyPackages)
            {
                WaitForCompletion(Client.Add(package));
            }
            WaitForUnityIdle();
            XRSettings.enabled = true;

            return report.summary.result == UnityEditor.Build.Reporting.BuildResult.Succeeded;
        }

        public static bool BuildClient()
        {
            var buildPath = BuildPath
                .Replace("[appType]", "Client")
                .Replace("[extension]", "apk");

            return BuildClient(buildPath);
        }

        public static bool BuildClient(string buildPath)
        {
            // == Set connection type ==
            SetConnectionType(UVRN_NetworkManager.ConnectionType.Client);

            // == Setup build ==
            Log($"Build path set to '{buildPath}'.");

            // exclude scenes with "ServerOnly" string in title
            var scenes = EditorBuildSettings.scenes
                .Where(s => s.enabled && !string.IsNullOrWhiteSpace(s.path) && !s.path.Contains("ServerOnly"))
                .Select(s => s.path)
                .ToArray();
            Log($"Building the '{string.Join(", ", scenes)}' scenes.");

            Log($"Switching build target");
            if (SwitchBuildTarget(BuildTarget.Android))
            {
                Log($"Target switched");
            }
            else
            {
                Log($"Error while switching target");
                return false;
            }

            var buildOptions = BuildOptions.CompressWithLz4;

            EditorUserBuildSettings.androidBuildSubtarget = MobileTextureSubtarget.ASTC;
            EditorUserBuildSettings.androidBuildSystem = AndroidBuildSystem.Gradle;
            EditorUserBuildSettings.androidBuildType = AndroidBuildType.Release;
            EditorUserBuildSettings.androidETC2Fallback = AndroidETC2Fallback.Quality32Bit;

            AssetDatabase.SaveAssets();

            // == Run build ==
            var report = BuildPipeline.BuildPlayer(new BuildPlayerOptions
            {
                locationPathName = buildPath,
                scenes = scenes,
                target = EditorUserBuildSettings.activeBuildTarget,
                targetGroup = BuildPipeline.GetBuildTargetGroup(EditorUserBuildSettings.activeBuildTarget),
                options = buildOptions
            });

            if (report.summary.result != UnityEditor.Build.Reporting.BuildResult.Succeeded)
            {
                Log($"Build failed - '{report.summary.result}'.");
            }
            else
            {
                Log("Build succeeded: " + report.summary.totalSize + " bytes");
            }

            return report.summary.result == UnityEditor.Build.Reporting.BuildResult.Succeeded;
        }

        public static void BuildAndRunServer()
        {
            var buildPath = BuildPath
                .Replace("[appType]", "Server")
                .Replace("[extension]", "exe");

            BuildServer(BuildTarget.StandaloneWindows64, buildPath);

            string fullPath = Path.GetFullPath(Path.Combine(Application.dataPath, "..", buildPath));

            Process.Start(fullPath);
        }

        #endregion

        #region Batch utils

        private static string GetOption(string optName)
        {
            var args = Environment.GetCommandLineArgs();
            var index = Array.IndexOf(args, optName);
            if (index < 0 || args.Length < index + 2 || string.IsNullOrWhiteSpace(args[index + 1]))
            {
                return null;
            }

            return args[index + 1];
        }

        private static string GetRequiredOption(string optName)
        {
            var option = GetOption(optName);
            if (option is null)
            {
                throw new InvalidOperationException($"The '{optName}' option is required.");
            }
            return option;
        }

        private static BuildOptions GetBuildOptions()
        {
            var buildOptionNames = (GetOption(BuildOptionsOption) ?? "").Split(',');
            var result = BuildOptions.None;
            foreach (var name in buildOptionNames)
            {
                if (Enum.TryParse<BuildOptions>(name, true, out var current))
                {
                    result |= current;
                }
                else if (!string.IsNullOrWhiteSpace(name))
                {
                    throw new NotSupportedException($"'{name}' is not a valid build option.");
                }
            }
            return result;
        }

        #endregion

        #region Build utils

        private static void WaitForCompletion(UnityEditor.PackageManager.Requests.Request request)
        {
            // Should be done better with UnityEditor.EditorApplication.update
            while (!request.IsCompleted)
            {
                Thread.Sleep(1000);
            }
        }

        private static void WaitForUnityIdle()
        {
            // Should be done better with UnityEditor.EditorApplication.update
            while (UnityEditor.EditorApplication.isCompiling || UnityEditor.EditorApplication.isUpdating)
            {
                Thread.Sleep(1000);
            }
        }

        private static void Log(string message)
        {
            message = "[Build] " + message;
            Console.WriteLine(message);
            UnityEngine.Debug.Log(message);
        }

        private static bool SwitchBuildTarget(BuildTarget buildTarget)
        {
            if (Application.isBatchMode)
            {
                if (buildTarget != EditorUserBuildSettings.activeBuildTarget)
                {
                    Log("I cannot change build target from a script, please use -buildTarget <name>");
                    return false;
                }
            }

            if (buildTarget == EditorUserBuildSettings.activeBuildTarget)
            {
                return true;
            }

            var targetGroup = BuildPipeline.GetBuildTargetGroup(buildTarget);

            /*
            var graphicsAPI = GraphicsDeviceType.Direct3D11;
            PlayerSettings.SetGraphicsAPIs(buildTarget, new[] { graphicsAPI });
            Log($"Build target graphics API set to '{graphicsAPI}'.");

            PlayerSettings.SetScriptingBackend(targetGroup, ScriptingImplementation.IL2CPP);
            Log($"Scripting backend set to '{PlayerSettings.GetScriptingBackend(targetGroup)}'.");
            */

            return EditorUserBuildSettings.SwitchActiveBuildTarget(targetGroup, buildTarget);
        }
        private static void SetConnectionType(UVRN_NetworkManager.ConnectionType connectionType)
        {
            if (!EditorUtils.EdiveSettings.BuildSettings)
            {
                return;
            }
            EditorUtils.EdiveSettings.BuildSettings.connectionType = connectionType;
        }

        private static bool BuildStartDialog()
        {
            return EditorUtility.DisplayDialog("Start the build",
                    "The build might take a while and the editor might seem unresponsibe meanwhile.\n" +
                    "You cannot stop the build once started.\n\n" +
                    "WARNING: The editor might seem like it is not working, but it is (trust me). Please wait for the final dialog.\n\n" +
                    "Are you sure you want to start the build?",
                    "Start",
                    "Cancel");
        }

        private static void BuildFinishedDialog(bool result, string path)
        {
            path = Path.GetFullPath(Path.Combine(Application.dataPath, "..", path));

            if (result)
            {
                if (EditorUtility.DisplayDialog("Build successfull",
                        "The build successfully finished!\n\nTarget path:\n" + path,
                        "Open target folder",
                        "OK")
                )
                {
                    Process.Start("explorer.exe", path);
                }
            }
            else
            {
                EditorUtility.DisplayDialog("Build failed",
                    "It seems like the build did not finish successfully. You can find more details in the log",
                    "Aw man, that sucks...");
            }
        }

        #endregion
    }
}
