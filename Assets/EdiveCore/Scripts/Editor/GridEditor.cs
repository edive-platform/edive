// Created by Vojtech Bruza
using Edive.Utils;
using UnityEditor;
using UnityEngine;

namespace EdiveEditor
{
    [CustomEditor(typeof(WorldGrid))]
    [CanEditMultipleObjects]
    public class GridEditor : Editor
    {
        WorldGrid myScript;
        private void OnEnable()
        {
            myScript = (WorldGrid)target;
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            GUILayout.Space(30);
            if (GUILayout.Button("Generate", new GUILayoutOption[] { GUILayout.Height(30) }))
            {
                myScript.Generate();
            }
            GUILayout.Space(30);
        }
    }
}