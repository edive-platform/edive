﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using UnityEngine.XR.Interaction.Toolkit;

namespace EdiveEditor
{

    public class AddTeleportationAreaScript : EditorWindow
    {

        [MenuItem("Edive/Legacy/Add TeleportArea Component")]
        public static void ShowWindow()
        {
            EditorWindow.GetWindow(typeof(AddTeleportationAreaScript));
        }

        public void OnGUI()
        {
            if (GUILayout.Button("Add..."))
            {
                AddComponent();
            }
        }
        private static void AddComponent()
        {
            int addedCount = 0;

            GameObject[] gos = FindObjectsByLayer(10);

            foreach (var go in gos)
            {
                TeleportationArea ta;
                //check, if there is such component
                ta = go.GetComponent<TeleportationArea>();

                if (ta == null)
                {
                    ObjectFactory.AddComponent<TeleportationArea>(go);
                    ta = go.GetComponent<TeleportationArea>();
                    addedCount++;
                }

                //TODO set references to custom reticle

            }
            Debug.Log(string.Format("Added: " + addedCount + " components"));
        }

        private static GameObject[] FindObjectsByLayer(int layer)
        {
            List<GameObject> validTransforms = new List<GameObject>();
            Transform[] objs = Resources.FindObjectsOfTypeAll<Transform>() as Transform[];
            for (int i = 0; i < objs.Length; i++)
            {
                //if (objs[i].hideFlags == HideFlags.None)
                {
                    if (objs[i].gameObject.layer == layer)
                    {
                        validTransforms.Add(objs[i].gameObject);
                    }
                }
            }
            return validTransforms.ToArray();
        }

    }
}