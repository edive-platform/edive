﻿// Created by Vojtech Bruza
using System.Collections.Generic;
using UnityEditor;

namespace EdiveEditor
{
    // partially from https://www.youtube.com/watch?v=c_3DXBrH-Is&ab_channel=GameDevGuide
    public class ExtendedEditorWindow : EditorWindow
    {
        protected SerializedObject serializedObject;
        protected SerializedProperty currentProperty;

        protected void DrawProperties(SerializedProperty property, bool drawChildren)
        {
            string lastPropertyPath = string.Empty;
            foreach (SerializedProperty p in property)
            {
                if (p.isArray && p.propertyType == SerializedPropertyType.Generic)
                {
                    EditorGUILayout.BeginHorizontal();
                    p.isExpanded = EditorGUILayout.Foldout(p.isExpanded, p.displayName);
                    EditorGUILayout.EndHorizontal();

                    if (p.isExpanded)
                    {
                        EditorGUI.indentLevel++;
                        DrawProperties(p, drawChildren);
                        EditorGUI.indentLevel--;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(lastPropertyPath) && p.propertyPath.Contains(lastPropertyPath)) continue;
                    lastPropertyPath = p.propertyPath;
                    EditorGUILayout.PropertyField(p, drawChildren);
                }
            }
        }

        protected void Apply()
        {
            serializedObject.ApplyModifiedProperties();
        }

        // based on https://answers.unity.com/questions/682932/using-generic-list-with-serializedproperty-inspect.html
        public static List<string> DeserializeList(SerializedProperty sp)
        {
            sp.Next(true); // skip generic field
            sp.Next(true); // advance to array size field

            // Get the array size
            int arrayLength = sp.intValue;

            sp.Next(true); // advance to first array index

            // Write values to list
            List<string> values = new List<string>(arrayLength);
            int lastIndex = arrayLength - 1;
            for (int i = 0; i < arrayLength; i++)
            {
                values.Add(sp.stringValue); // copy the value to the list
                if (i < lastIndex) sp.Next(false); // advance without drilling into children
            }

            return values;
        }
    }
}