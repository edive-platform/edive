﻿// Created by Vojtech Bruza
using Edive.UI.Keyboard;
using UnityEditor;
using UnityEngine;


namespace EdiveEditor.KeyboardEditor
{
    [CustomEditor(typeof(Keyboard))]
    [CanEditMultipleObjects]
    public class KeyboardEditor : Editor
    {
        Keyboard myScript;
        private void OnEnable()
        {
            myScript = (Keyboard)target;
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            if (GUILayout.Button("Send"))
            {
                myScript.Send();
            }
        }
    }
}