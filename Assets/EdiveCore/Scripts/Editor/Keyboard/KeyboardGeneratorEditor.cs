﻿// Created by Vojtech Bruza
using Edive.UI.Keyboard;
using UnityEditor;
using UnityEngine;


namespace EdiveEditor.KeyboardEditor
{
    [CustomEditor(typeof(KeyboardGenerator))]
    [CanEditMultipleObjects]
    public class KeyboardGeneratorEditor : Editor
    {
        KeyboardGenerator myScript;
        private void OnEnable()
        {
            myScript = (KeyboardGenerator)target;
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            if (GUILayout.Button("Generate"))
            {
                myScript.GenerateKeyboard();
            }
        }
    }
}