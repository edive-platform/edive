﻿// Created by Vojtech Bruza
using Edive.UI.Keyboard;
using UnityEditor;
using UnityEngine;

namespace EdiveEditor.KeyboardEditor
{
    [CustomEditor(typeof(Key))]
    [CanEditMultipleObjects]
    public class KeyEditor : Editor
    {
        Key myScript;
        private void OnEnable()
        {
            myScript = (Key)target;
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            if (GUILayout.Button("Click"))
            {
                myScript.Click();
            }
        }
    }
}