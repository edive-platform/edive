// Created by Vojtech Bruza
using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR
namespace EdiveEditor.Setup
{
    /// <summary>
    /// To be able to open by double-click
    /// </summary>
    public class AssetHandler
    {
        public static bool OpenEditor(int instanceID, int line)
        {
            EdiveSetupSO obj = EditorUtility.InstanceIDToObject(instanceID) as EdiveSetupSO;
            if (obj != null)
            {
                EdiveSetupEditorWindow.ShowAndOpen(obj);
                return true;
            }
            return false;
        }
    }

    [CustomEditor(typeof(EdiveSetupSO))]
    public class EdiveSetupEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            if (GUILayout.Button("Open Editor"))
            {
                EdiveSetupEditorWindow.ShowAndOpen((EdiveSetupSO)target);
            }
        }
    }
}
#endif
