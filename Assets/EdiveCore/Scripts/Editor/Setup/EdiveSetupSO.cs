// Created by Vojtech Bruza
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Net;
using UnityEditor;
using UnityEngine;
using static EdiveEditor.EditorUtils;

#if UNITY_EDITOR
namespace EdiveEditor.Setup
{
    [Serializable]
    public struct EdiveEnvironment
    {
        public string name;
        public string directoryName;
        public string url;
        public string dataURL;
        [Tooltip("The urls of the environments that this environment depends on. " +
            "When downloading this environment, all dependencies are also downloaded.")]
        public List<string> dependencies;
    }

    [CreateAssetMenu(fileName = "EdiveSetupSO", menuName = "Edive/EdiveSetup", order = 5)]
    public class EdiveSetupSO : ScriptableObject
    {
        public string mirrorVersion = "40.0.9";
        public string environmentsFolderName = "Environments";
        public List<EdiveEnvironment> environments = new List<EdiveEnvironment>();
        private string EnvironmentsDirectory => Path.Combine(Application.dataPath, environmentsFolderName);

        public void DownloadMirror(string folder)
        {
            var progress = new ProgressBar("Downloading Mirror", 3);

            string dir = EditorUtils.GetTempLocation();
            string zip = Path.Combine(dir, "mirror.zip");
            string extracted = Path.Combine(dir, "mirror");
            string source = Path.Combine(extracted, $"Mirror-{mirrorVersion}", "Assets", "Mirror");
            string destination = Path.Combine(Application.dataPath, Path.Combine(folder, "Mirror"));

            progress.Step("Downloading zip from GitHub");
            using (var client = new WebClient())
            {
                // I dont like this hardcoded link...however this should be ok since Mirror really should not change
                string mirrorAddress = $"https://github.com/vis2k/Mirror/archive/refs/tags/v{mirrorVersion}.zip";
                client.DownloadFile(mirrorAddress, zip);
            }

            progress.Step("Extracting");
            ZipFile.ExtractToDirectory(zip, extracted);

            File.Copy(source + ".meta", destination + ".meta");
            EditorUtils.CopyDir(source, destination);

            progress.Step("Completed");
        }

        public void DownloadAssets(string remoteResourceURL, string targetFolder)
        {
            if (string.IsNullOrWhiteSpace(remoteResourceURL))
            {
                Debug.LogError("No file name specified.");
                return;
            }
            Uri uri = new Uri(remoteResourceURL);
            string filename = Path.GetFileName(uri.LocalPath);
            var targetTmpFilePath = Path.Combine(GetTempLocation(), filename);

            var progress = new ProgressBar("Downloading Assets", 4);

            progress.Step("Downloading zip from the server " + filename);
            using (var client = new WebClient())
            {
                try
                {
                    Debug.Log("Downloading from " + remoteResourceURL + " to " + targetTmpFilePath);
                    client.DownloadFile(remoteResourceURL, targetTmpFilePath);
                }
                catch (Exception e)
                {
                    Debug.Log(e);
                }
            }

            // TODO better behavior when something fails?

            var targetDir = Path.Combine(EnvironmentsDirectory, targetFolder);
            if (Path.GetExtension(targetTmpFilePath).Equals(".zip"))
            {
                try
                {
                    progress.Step("Extracting");
                    ZipFile.ExtractToDirectory(targetTmpFilePath, targetDir);
                }
                catch (Exception e)
                {
                    Debug.Log(e);
                }
                try
                {
                    progress.Step("Deleting temp zip file");
                    File.Delete(targetTmpFilePath);
                }
                catch (Exception e)
                {
                    Debug.Log(e);
                }
            }
            else
            {
                progress.Step("No need to extract");
                File.Copy(targetTmpFilePath, targetDir);
                progress.Step("Skipping");
            }



            progress.Step("Completed. Refreshing assets...");
            AssetDatabase.Refresh();
        }

        /// <summary>
        /// Clone the environment from git to the target directory.
        /// </summary>
        /// <param name="fromURL_subdir"></param>
        /// <param name="targetDirectory"></param>
        public void CloneEnvironmentGitModule(string url, string targetDirectory)
        {
            if (string.IsNullOrWhiteSpace(url))
            {
                Debug.Log("No URL to clone from specified.");
                return;
            }
            if (url.Contains(" "))
            {
                Debug.Log("The provided URL must not contain whitespaces \"" + url + "\".");
                return;
            }
            if (string.IsNullOrWhiteSpace(targetDirectory))
            {
                Debug.Log("No directory to clone to specified.");
                return;
            }

            Uri uri = new Uri(url);
            string moduleName = Path.GetFileNameWithoutExtension(uri.LocalPath);
        
            var progress = new ProgressBar("Downloading module", 2);
            var targetPath = Path.Combine(EnvironmentsDirectory, targetDirectory); // git needs an empty folder to clone into

            if (Directory.Exists(targetPath))
            {
                Debug.Log("The environment " + targetDirectory + "is already downloaded.");
                return;
            }

            var cmd = $@"git clone ""{url}"" ""{targetPath}""";
            Debug.Log("Command to be called:\n " + cmd);
            progress.Step("Cloning " + moduleName);
            // TODO add a log (the return value not working)
            CommandLineUtils.CallCommand(cmd);

            progress.Step("Completed. Refreshing assets...");
            AssetDatabase.Refresh();
        }
    }
}
#endif
