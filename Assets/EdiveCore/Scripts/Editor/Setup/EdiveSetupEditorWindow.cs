// Created by Vojtech Bruza
using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR
namespace EdiveEditor.Setup
{
    public class EdiveSetupEditorWindow : ExtendedEditorWindow
    {
        private static string windowTitle = "Edive Setup";
        private static Vector2 windowMinSize = new Vector2(600, 120);
        /// <summary>
        /// To be able to reopen lastly selected
        /// </summary>
        private const string editorPrefsKey = "LastEdiveSetupSOPath";

        private string mirrorFolder = "_NonVersioned";

        /// <summary>
        /// Store the loaded data instance (non-serialized)
        /// </summary>
        private EdiveSetupSO nonSerializedDataHolder = null;
        /// <summary>
        /// Store the scroll position
        /// </summary>
        private Vector2 scrollPosition;

        private bool loaded = false;

        [MenuItem("Edive/Project Setup...", false, 200)]
        public static void ShowWindow()
        {
            var dataPath = EditorPrefs.GetString(editorPrefsKey);
            var lastOpenData = AssetDatabase.LoadAssetAtPath<EdiveSetupSO>(dataPath);
            ShowAndOpen(lastOpenData);
        }

        public static void ShowAndOpen(EdiveSetupSO data)
        {
            var window = GetWindow<EdiveSetupEditorWindow>(windowTitle);
            window.minSize = windowMinSize;
            if (data != null) window.LoadData(data);
        }

        private void LoadData(EdiveSetupSO data)
        {
            if (data == null)
            {
                Debug.LogError("No data provided.");
                return;
            }
            nonSerializedDataHolder = data;
            serializedObject = new SerializedObject(data);
            string dataPath = AssetDatabase.GetAssetPath(nonSerializedDataHolder);
            EditorPrefs.SetString(editorPrefsKey, dataPath);
            loaded = true;
        }

        private void OnGUI()
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label("Current Data Holder");
            var newData = EditorGUILayout.ObjectField(nonSerializedDataHolder, typeof(EdiveSetupSO), false) as EdiveSetupSO;
            
            if (nonSerializedDataHolder != newData)
            {
                LoadData(newData);
            }

            GUILayout.EndHorizontal();
            if (nonSerializedDataHolder == null)
            {
                EditorGUILayout.HelpBox("You have no setup data assigned. Please, download or create new editor setup data that contain the downloadable modules. Then assign the data object using the field above.", MessageType.Error);
                return;
            }

            if (!loaded)
            {
                LoadData(nonSerializedDataHolder);
            }
            if (nonSerializedDataHolder == null || serializedObject == null)
            {
                return;
            }

            serializedObject.Update();

            // mirror

            GUILayout.Space(12);
            GUILayout.Label("Modules", EditorStyles.boldLabel);

            string labelName = "Mirror Version";
            string tooltip = "The version of Mirror to download";

            scrollPosition = GUILayout.BeginScrollView(scrollPosition);
            GUILayout.BeginHorizontal();
            GUILayout.BeginVertical();

            SerializedProperty mirrorVersion = serializedObject.FindProperty(nameof(nonSerializedDataHolder.mirrorVersion));
            EditorGUILayout.PropertyField(mirrorVersion, new GUIContent(labelName, tooltip), true);

            mirrorFolder = EditorGUILayout.TextField("Mirror download folder: ", mirrorFolder);

            if (GUILayout.Button("Download Mirror"))
            {
                nonSerializedDataHolder.DownloadMirror(mirrorFolder);
            } 

            GUILayout.EndVertical();
            GUILayout.EndHorizontal();

            GUILayout.Space(12);
            GUILayout.BeginHorizontal();
            GUILayout.Label("Environments", EditorStyles.boldLabel);

            bool cloneAll = false;
            bool downloadAll = false;
            if (GUILayout.Button("Clone All"))
            {
                cloneAll = true;
                this.Close();
            }
            else if (GUILayout.Button("Download All"))
            {
                downloadAll = true;
                this.Close();
            }

            GUILayout.EndHorizontal();

            // git repos

            // TODO deleting with using this https://answers.unity.com/questions/682932/using-generic-list-with-serializedproperty-inspect.html

            uint envCount = 0;
            foreach (SerializedProperty environment in serializedObject.FindProperty(nameof(nonSerializedDataHolder.environments)))
            {
                GUILayout.BeginHorizontal();

                var env = new EdiveEnvironment();
                env.name = environment.FindPropertyRelative(nameof(env.name)).stringValue;
                env.directoryName = environment.FindPropertyRelative(nameof(env.directoryName)).stringValue;
                env.url = environment.FindPropertyRelative(nameof(env.url)).stringValue;
                env.dataURL = environment.FindPropertyRelative(nameof(env.dataURL)).stringValue;
                env.dependencies = ExtendedEditorWindow.DeserializeList(environment.FindPropertyRelative(nameof(env.dependencies)));

                labelName = string.IsNullOrEmpty(env.name) ? "Env" : env.name;
                tooltip = "Environment to download";
                // TODO rewrite this using custom labels and text fields instead of the property field
                EditorGUILayout.PropertyField(environment, new GUIContent(labelName, tooltip), true);

                if (GUILayout.Button("Clone Repository") || cloneAll)
                {
                    if (string.IsNullOrEmpty(env.url) || string.IsNullOrEmpty(env.directoryName))
                    {
                        Debug.Log("Cannot clone empty.");
                        continue;
                    }
                    // TODO are the non-serialized realy data up-to-date?
                    DownloadEnvironmentiWithDependenciesRecursive(env, nonSerializedDataHolder.environments);
                    this.Close();
                }

                if (GUILayout.Button("Download Data") || downloadAll)
                {
                    if (string.IsNullOrEmpty(env.dataURL) || string.IsNullOrEmpty(env.directoryName))
                    {
                        Debug.Log("Cannot download empty.");
                        continue;
                    }
                    nonSerializedDataHolder.DownloadAssets(env.dataURL, Path.Combine("_Data", env.directoryName));
                    this.Close();
                }
                GUILayout.EndHorizontal();
                envCount++;
            }

            GUILayout.Label("Total environments: " + envCount);

            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Add New Environment"))
            {
                nonSerializedDataHolder.environments.Add(new EdiveEnvironment());
            }
            GUILayout.EndHorizontal();

            GUILayout.Space(12);

            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Save Changes"))
            {
                SaveAssets();
            }
            GUILayout.EndHorizontal();

            GUILayout.EndScrollView();

            Apply();
        }

        //private void DownloadDependencies(SerializedProperty environment)
        //{
        //    var dependencies = environment.FindPropertyRelative("dependencies");
        //    foreach (SerializedProperty dependency in dependencies)
        //    {
        //        string dependencyName = dependency.stringValue;
        //        Debug.Log("Checking dependency: " + dependencyName);
        //        if (string.IsNullOrEmpty(dependencyName))
        //        {
        //            Debug.Log("Cannot download empty dependency.");
        //            continue;
        //        }
        //        var matchedDependency = nonSerializedData.environments.Find(
        //            new System.Predicate<EdiveEnvironment>(
        //                (e) =>
        //                {
        //                    return e.directoryName.Equals(dependencyName);
        //                }));
        //        if (string.IsNullOrEmpty(matchedDependency.name)) // is null
        //        {
        //            Debug.Log("Skipping. No such dependency found: " + dependencyName);
        //            continue;
        //        }
        //        Debug.Log("Downloading dependency: " + dependencyName);
        //        nonSerializedData.CloneEnvironmentGitModule(matchedDependency.url, matchedDependency.directoryName);
        //    }
        //}

        private void DownloadEnvironmentiWithDependenciesRecursive(EdiveEnvironment environment, List<EdiveEnvironment> allEnvironments)
        {
            foreach (string dependencyName in environment.dependencies)
            {
                foreach (EdiveEnvironment env in allEnvironments)
                {
                    if (dependencyName == env.url)
                    {
                        Debug.Log("Preparing to download dependencies for " + dependencyName);
                        DownloadEnvironmentiWithDependenciesRecursive(env, allEnvironments);
                        break;
                    }
                }
            }
            Debug.Log("Downloading " + environment.name);
            nonSerializedDataHolder.CloneEnvironmentGitModule(environment.url, environment.directoryName);
        }

        private void OnLostFocus()
        {
            SaveAssets();
        }

        private void SaveAssets()
        {
            if (nonSerializedDataHolder == null) return;
            AssetDatabase.SaveAssetIfDirty(nonSerializedDataHolder);
            AssetDatabase.Refresh();
        }
    }
}
#endif