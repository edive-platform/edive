// Created by Vojtech Bruza
using Edive.App.Settings;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Windows;

namespace EdiveEditor
{
    /// <summary>
    /// Can be used to control the build settings.
    /// These settings will be applied to build and runtime.
    /// </summary>
    public class EdiveEditorWindow : EditorWindow
    {
        private EdiveSettingsObject ediveSettings;
        private Vector2 scrollPosition;
        private bool overrideDefaultServerSettings;

        [MenuItem("Edive/Build Settings...", false, -20)]
        public static void ShowWindow()
        {
            // Show existing window instance. If one doesn't exist, make one.
            GetWindow(typeof(EdiveEditorWindow));
        }

        private void OnGUI()
        {
            int spacing = 10;
            float spacingMult = 1.5f;
            titleContent.text = "Edive Settings";
            titleContent.tooltip = "Use these to setup before build";

            //ediveSettings = EditorGUILayout.ObjectField("Edive Settings", ediveSettings, typeof(EdiveSettingsObject), false) as EdiveSettingsObject;
            ediveSettings = EditorUtils.EdiveSettings;

            if (ediveSettings == null)
            {
                EditorGUILayout.HelpBox("Please create and assign the Edive settings scriptable object first.", MessageType.Info);
                return;
            }

            scrollPosition = GUILayout.BeginScrollView(scrollPosition);


            GUILayout.Label("Base Settings", EditorStyles.boldLabel);

            PlayerSettings.productName = EditorGUILayout.TextField(new GUIContent("Product Name", "The same as in the player settings (but needs to be edited from here to have the package name correctly overriden)"), PlayerSettings.productName);

            // force update package since it is not updated automatically when being edited from this script (to avoid conflicting Package name)
            // this cancels the overide toggle, however it is overriden anyway from here
            PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, PlayerSettings.companyName + "." + PlayerSettings.productName);

            PlayerSettings.bundleVersion = EditorGUILayout.TextField(new GUIContent("Version", "The same as in the player settings"), PlayerSettings.bundleVersion);
            GUILayout.Space(spacing * spacingMult);
            ediveSettings.MainScenePath = EditorGUILayout.TextField(new GUIContent("Main Scene Path", "The path to the main networking scene"), ediveSettings.MainScenePath);
            GUILayout.Space(spacing * spacingMult);

            GUILayout.Label("Build Settings", EditorStyles.boldLabel);

            if (ediveSettings.BuildSettings == null)
            {
                ediveSettings.BuildSettings = EditorGUILayout.ObjectField(ediveSettings.BuildSettings, typeof(EdiveBuildSettingsObject), false) as EdiveBuildSettingsObject;
            }
            else
            {
                ediveSettings.BuildSettings.connectionType = (UVRN.UVRN_NetworkManager.ConnectionType)EditorGUILayout.EnumPopup(new GUIContent("Connection Type", "Server or client build"),
                                                                                    ediveSettings.BuildSettings.connectionType);
                ediveSettings.BuildSettings.targetPlatform = (EdiveSettingsObject.TargetPlatform)EditorGUILayout.EnumPopup(new GUIContent("Target Platform", "Will this build be controlled using VR (headset + controllers) or on the desktop (keyboard + mouse)"),
                                                                                    ediveSettings.BuildSettings.targetPlatform);
                ediveSettings.BuildSettings.isDev = EditorGUILayout.Toggle(new GUIContent("Is Dev", "Is this a development build? E.g. the development server will not be visible in release builds"),
                                                                                    ediveSettings.BuildSettings.isDev);
            }

            if (ediveSettings.BuildSettings != null)
            {
                SerializedObject so = new SerializedObject(ediveSettings.BuildSettings);
                // TODO rewrite "all" what makes sense using the serialized property to be able to save changes to disk...
                SerializedProperty stringsProperty = so.FindProperty(nameof(ediveSettings.BuildSettings.clientOnlyPackages));
                EditorGUILayout.PropertyField(stringsProperty,
                    new GUIContent("Client-Only Packages", "Packages thet will be removed when building server and reapplied"), true);
                so.ApplyModifiedProperties(); // Remember to apply modified properties (at the and of on gui)
                GUILayout.Space(spacing);
            }

            GUILayout.Label("App Settings", EditorStyles.boldLabel);
            if (ediveSettings.RuntimeSettings == null)
            {
                ediveSettings.RuntimeSettings = EditorGUILayout.ObjectField(ediveSettings.RuntimeSettings, typeof(EdiveRuntimeSettings), false) as EdiveRuntimeSettings;
                EditorGUILayout.HelpBox("Please create and assign the app setup scriptable object first. Should be a non-versioned file.", MessageType.Info);
            }
            else
            {
                ediveSettings.RuntimeSettings.serverManagerUrl = EditorGUILayout.TextField(new GUIContent("Server Code Manager URL", "URL to the server code manager"),
                                                                                    ediveSettings.RuntimeSettings.serverManagerUrl);
                ediveSettings.RuntimeSettings.serverListingManagerUrl = EditorGUILayout.TextField(new GUIContent("Server List Manager Url", "URL to API root of online server list manager"),
                                                                                    ediveSettings.RuntimeSettings.serverListingManagerUrl);
                ediveSettings.RuntimeSettings.serverListingManagerSecret = EditorGUILayout.TextField(new GUIContent("Listing Server Manager Secret", "Server manager authorization secret - required for the server listing"),
                                                                                    ediveSettings.RuntimeSettings.serverListingManagerSecret);
                GUILayout.Space(spacing);
            }

            GUILayout.Label("Server Settings", EditorStyles.boldLabel);
            if (ediveSettings.ServerSettings == null)
            {
                ediveSettings.ServerSettings = EditorGUILayout.ObjectField(ediveSettings.ServerSettings, typeof(EdiveServerSettingsObject), false) as EdiveServerSettingsObject;
            }
            else
            {
                ediveSettings.ServerSettings.ServerSettingsFileName = EditorGUILayout.TextField(new GUIContent("Server Settings File Name", "Name of a file with server settings"),
                                                                                    ediveSettings.ServerSettings.ServerSettingsFileName);
                overrideDefaultServerSettings = EditorGUILayout.BeginToggleGroup(new GUIContent("Override Default Server Settings", "Sets the default values for the server settings file that can be modified in build."), overrideDefaultServerSettings);
                if (overrideDefaultServerSettings)
                {
                    ediveSettings.ServerSettings.serverTitle.defaultValue = EditorGUILayout.TextField(new GUIContent("Server Title", ediveSettings.ServerSettings.serverTitle.tooltip),
                                                                                        ediveSettings.ServerSettings.serverTitle.defaultValue);
                    ediveSettings.ServerSettings.localIP.defaultValue = EditorGUILayout.Toggle(new GUIContent("Local IP", ediveSettings.ServerSettings.localIP.tooltip),
                                                                                        ediveSettings.ServerSettings.localIP.defaultValue);
                    ediveSettings.ServerSettings.autoRegisterServerOnline.defaultValue = EditorGUILayout.Toggle(new GUIContent("Auto-register Server Online", ediveSettings.ServerSettings.autoRegisterServerOnline.tooltip),
                                                                                        ediveSettings.ServerSettings.autoRegisterServerOnline.defaultValue);
                    ediveSettings.ServerSettings.port.defaultValue = (ushort)EditorGUILayout.IntField(new GUIContent("Port", ediveSettings.ServerSettings.port.tooltip),
                                                                                        ediveSettings.ServerSettings.port.defaultValue);
                    ediveSettings.ServerSettings.updateFrequency.defaultValue = EditorGUILayout.IntSlider(new GUIContent("Update Frequency", ediveSettings.ServerSettings.updateFrequency.tooltip),
                                                                                        ediveSettings.ServerSettings.updateFrequency.defaultValue, 10, 180);
                }
                EditorGUILayout.EndToggleGroup();
                GUILayout.Space(spacing);
            }

            GUILayout.Label("Client Settings", EditorStyles.boldLabel);
            if (ediveSettings.ClientSettings == null)
            {
                ediveSettings.ClientSettings = EditorGUILayout.ObjectField(ediveSettings.ClientSettings, typeof(EdiveClientSettingsObject), false) as EdiveClientSettingsObject;
            }
            else
            {
                string text = "Auto-connect Client On Server Found";
                ediveSettings.ClientSettings.autoConnectClientOnServerFound = EditorGUILayout.Toggle(new GUIContent(text,
                                                                                            text + ": Connects to the registered server server with same version (if any)."),
                                                                                            ediveSettings.ClientSettings.autoConnectClientOnServerFound);
            }

            GUILayout.Space(2 * spacing);

            if (ediveSettings.BuildSettings != null)
            {
                GUILayout.Label("Run");
                GUILayout.BeginHorizontal();
                if (GUILayout.Button("Run Client"))
                {
                    ediveSettings.BuildSettings.targetPlatform = EdiveSettingsObject.TargetPlatform.Desktop;
                    ediveSettings.BuildSettings.connectionType = UVRN.UVRN_NetworkManager.ConnectionType.Client;
                    Run();
                }
                if (GUILayout.Button("Run Server", GUILayout.Width(position.width / 2)))
                {
                    ediveSettings.BuildSettings.targetPlatform = EdiveSettingsObject.TargetPlatform.Desktop;
                    ediveSettings.BuildSettings.connectionType = UVRN.UVRN_NetworkManager.ConnectionType.Server;
                    Run();
                }
                GUILayout.EndHorizontal();

                GUILayout.Label("Build Client");
                GUILayout.BeginHorizontal();
                if (GUILayout.Button("Build VR Client...", GUILayout.Width(position.width / 2)))
                {
                    // Automatic build directly from Edive Settings window (added by O.Kvarda) //
                    if (File.Exists("Builds/AutomatedBuilds/VR/" + PlayerSettings.productName + ".apk"))
                    {

                        if (EditorUtility.DisplayDialog("Caution, build collision!", "Build with the same name is already present in the output folder. In order to prevent possible collision of the already generated build with the newly generated one, the previous build must be deleted.\n\nBy pressing PROCEED, you acknowledge this, and the previous build will be DELETED.\n\n(Alternatively, you can change the name of the build and nothing will be deleted.)", "Proceed", "Cancel"))
                        {
                            File.Delete("Builds/AutomatedBuilds/VR/" + PlayerSettings.productName + ".apk");
                            Directory.Delete("Builds/AutomatedBuilds/VR/" + PlayerSettings.productName + "_BurstDebugInformation_DoNotShip");
                            buildVR();
                        }

                    }
                    else
                    {
                        buildVR();
                    }
                    GUIUtility.ExitGUI();
                }
                if (GUILayout.Button("Build Desktop Client..."))
                {
                    // Automatic build directly from Edive Settings window (added by O.Kvarda) //
                    if (Directory.Exists("Builds/AutomatedBuilds/Desktop/" + PlayerSettings.productName))
                    {

                        if (EditorUtility.DisplayDialog("Caution, build collision!", "Build with the same name is already present in the output folder. In order to prevent possible collision of the already generated build with the newly generated one, the previous build must be deleted.\n\nBy pressing PROCEED, you acknowledge this and the previous build will be DELETED.\n\n(Alternatively, you can change the name of the build and nothing will be deleted.)", "Proceed", "Cancel"))
                        {
                            Directory.Delete("Builds/AutomatedBuilds/Desktop/" + PlayerSettings.productName + "/");
                            buildDesktop();
                        }

                    }
                    else
                    {
                        buildDesktop();
                    }
                    GUIUtility.ExitGUI();
                }
                GUILayout.EndHorizontal();

                GUILayout.Label("Build Server");
                GUILayout.BeginHorizontal();
                if (GUILayout.Button("Automated Build Desktop Server"))
                {
                    // Automatic build directly from Edive Settings window (added by O.Kvarda) //
                    if (Directory.Exists("Builds/AutomatedBuilds/Server/" + PlayerSettings.productName + "_" + PlayerSettings.bundleVersion))
                    {

                        if (EditorUtility.DisplayDialog("Caution, build collision!", "Build with the same name is already present in the output folder. In order to prevent possible collision of the already generated build with the newly generated one, the previous build must be deleted.\n\nBy pressing PROCEED, you acknowledge this and the previous build will be DELETED.\n\n(Alternatively, you can change the name/version of the build and nothing will be deleted.)", "Proceed", "Cancel"))
                        {
                            Directory.Delete("Builds/AutomatedBuilds/Server/" + PlayerSettings.productName + "_" + PlayerSettings.bundleVersion);
                            buildServer();
                        }

                    }
                    else
                    {
                        buildServer();
                    }
                    GUIUtility.ExitGUI();
                }
                GUILayout.EndHorizontal();
            }


            GUILayout.Space(2 * spacing);

            GUILayout.Label("Utils");
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Locate Settings Asset", GUILayout.Width(position.width / 2)))
            {
                EditorGUIUtility.PingObject(ediveSettings);
            }
            if (GUILayout.Button(new GUIContent("Save For Everyone", "Right now only works when changing the packages (WIP)")))
            {
                AssetDatabase.SaveAssets();
            }
            GUILayout.EndHorizontal();

            GUILayout.EndScrollView();
            GUILayout.Space(spacing * spacingMult);
        }

        public void buildVR()
        {
            ediveSettings.BuildSettings.targetPlatform = EdiveSettingsObject.TargetPlatform.VR;
            ediveSettings.BuildSettings.connectionType = UVRN.UVRN_NetworkManager.ConnectionType.Client;
            BuildPlayerWindow.ShowBuildPlayerWindow();
            EditorUserBuildSettings.standaloneBuildSubtarget = StandaloneBuildSubtarget.Player;
            EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Android, BuildTarget.Android);
            static string[] GetScenePaths()
            {
                string[] scenes = new string[EditorBuildSettings.scenes.Length];
                for (int i = 0; i < scenes.Length; i++)
                {
                    scenes[i] = EditorBuildSettings.scenes[i].path;
                }
                return scenes;
            }
            BuildPipeline.BuildPlayer(GetScenePaths(), "Builds/AutomatedBuilds/VR/" + PlayerSettings.productName + ".apk", BuildTarget.Android, BuildOptions.None);
            // Automatic build directly from Edive Settings window (added by O.Kvarda) //
        }

        public void buildDesktop()
        {
            ediveSettings.BuildSettings.targetPlatform = EdiveSettingsObject.TargetPlatform.Desktop;
            ediveSettings.BuildSettings.connectionType = UVRN.UVRN_NetworkManager.ConnectionType.Client;
            BuildPlayerWindow.ShowBuildPlayerWindow();
            EditorUserBuildSettings.standaloneBuildSubtarget = StandaloneBuildSubtarget.Player;
            EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Standalone, BuildTarget.StandaloneWindows64);
            static string[] GetScenePaths()
            {
                string[] scenes = new string[EditorBuildSettings.scenes.Length];
                for (int i = 0; i < scenes.Length; i++)
                {
                    scenes[i] = EditorBuildSettings.scenes[i].path;
                }
                return scenes;
            }
            BuildPipeline.BuildPlayer(GetScenePaths(), "Builds/AutomatedBuilds/Desktop/" + "/" + PlayerSettings.productName + "/" + PlayerSettings.productName + ".exe", BuildTarget.StandaloneWindows64, BuildOptions.None);
            // Automatic build directly from Edive Settings window (added by O.Kvarda) //
        }

        public void buildServer()
        {
            EditorUserBuildSettings.standaloneBuildSubtarget = StandaloneBuildSubtarget.Server;
            EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Standalone, BuildTarget.StandaloneWindows64);
            Build.BuildServerWrapper(BuildTarget.StandaloneWindows64);
            // Automatic build directly from Edive Settings window (added by O.Kvarda) //
        }

        private void Run()
        {
            EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
            EditorSceneManager.OpenScene(ediveSettings.MainScenePath);
            EditorApplication.isPlaying = true;
        }
    }
}
