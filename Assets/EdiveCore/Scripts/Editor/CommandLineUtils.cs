// Created by Vojtech Bruza
using System.Diagnostics;
using System.Threading;

public static class CommandLineUtils
{
    public static void CallCommandAsync(string cmd)
    {
        var thread = new Thread(delegate () { CallCommand(cmd); });
        thread.Start();
    }

    public static string CallCommand(string cmd)
    {
        var processInfo = new ProcessStartInfo()
        {
            // "c" to actually run the command and then terminate https://ss64.com/nt/cmd.html
            FileName = "cmd.exe /c",
            Arguments = cmd,
            RedirectStandardError = true,
            RedirectStandardOutput = true,
            CreateNoWindow = true,
            UseShellExecute = false,
        };

        var process = Process.Start(processInfo);
        process.WaitForExit();
        var output = process.StandardOutput.ReadToEnd();
        process.Close();
        return output;
    }
}
