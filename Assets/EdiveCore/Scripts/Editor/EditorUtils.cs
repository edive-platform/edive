﻿// Created by Vojtech Bruza
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using UVRN;
using System.IO;
using System.Threading;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.SceneManagement;
using Unity.EditorCoroutines.Editor;
using System.Collections;
using Edive.App.Settings;
using UnityEditor.Compilation;

namespace EdiveEditor
{
#if UNITY_EDITOR
    // TODO scriptable object to save the data persistently

    // ensure class initializer is called whenever scripts recompile
    [InitializeOnLoadAttribute]
    public static class EditorUtils
    {
        //// register an event handler when the class is initialized
        //static EditorUtils()
        //{
        //    EditorApplication.playModeStateChanged += EditorApplication_playModeStateChanged;
        //}

        //// a bit of a hack to be able to unregister the event hanler
        //private sealed class Destructor
        //{
        //    ~Destructor()
        //    {
        //        // One time only destructor.
        //        EditorApplication.playModeStateChanged -= EditorApplication_playModeStateChanged;
        //    }
        //}
        //private static readonly Destructor Finalise = new Destructor();

        public static readonly string settingsPath = "Assets/EdiveCore/Settings/EdiveSettings/EdiveSettings.asset";
        private static EdiveSettingsObject m_ediveSettings;
        //public static readonly string editorSettingsPath = "Assets/Settings/EdiveSettings/Editor/EdiveEditorSettings.asset";
        //private static EdiveEditorSettingsSO m_ediveEditorSettings;

        private static Vector3 copiedTransformPosition;
        private static Quaternion copiedTransformRotation;

        static Component[] copiedComponents;

        public static EdiveSettingsObject EdiveSettings
        {
            get
            {
                if (!m_ediveSettings)
                {
                    m_ediveSettings = AssetDatabase.LoadAssetAtPath<EdiveSettingsObject>(settingsPath);
                    if (!m_ediveSettings) Debug.LogError("You need to create and put the edive settings to: " + settingsPath);
                }
                return m_ediveSettings;
            }
        }

        //public static EdiveEditorSettingsSO EdiveEditorSettings
        //{
        //    get
        //    {
        //        if (!m_ediveEditorSettings)
        //        {
        //            m_ediveEditorSettings = AssetDatabase.LoadAssetAtPath<EdiveEditorSettingsSO>(editorSettingsPath);
        //            if (!m_ediveEditorSettings) Debug.LogError("You need to create and put the edive editor settings to: " + editorSettingsPath);
        //        }
        //        return m_ediveEditorSettings;
        //    }
        //}

#region Edive

        [MenuItem("Edive/Run Server", false, 1)]
        public static void RunServer()
        {
            Run(UVRN_NetworkManager.ConnectionType.Server, EdiveSettingsObject.TargetPlatform.Desktop);
        }

        [MenuItem("Edive/Run Client Desktop", false, 2)]
        public static void RunDesktopClient()
        {
            Run(UVRN_NetworkManager.ConnectionType.Client, EdiveSettingsObject.TargetPlatform.Desktop);
        }

        [MenuItem("Edive/Run Client VR", false, 3)]
        public static void RunVRClient()
        {
            Run(UVRN_NetworkManager.ConnectionType.Client, EdiveSettingsObject.TargetPlatform.VR);
        }


        [MenuItem("Edive/Utils/Recompile", false, 80)]
        private static void RefreshAssets()
        {
            // This kind of freezez the editor: EditorUtility.RequestScriptReload();
            // TODO update Unity to be able to clean cache too
            CompilationPipeline.RequestScriptCompilation();
        }

        [MenuItem("Edive/Utils/Create AttachPoints", false, 100)]
        public static void CreateAttachPoints()
        {
            int i = 0;
            int j = 0;
            foreach (var o in Selection.objects)
            {
                var go = (o as GameObject);
                XRGrabInteractable interactable = go?.GetComponent<XRGrabInteractable>();
                if (interactable)
                {
                    var name = "AttachPoint";
                    // try to find the child
                    var attachTransform = go.transform.Find(name)?.transform;
                    if (!attachTransform)
                    {
                        attachTransform = new GameObject().transform;
                        attachTransform.name = name;
                    }
                    var col = go.GetComponentInChildren<Collider>();
                    if (col) attachTransform.position = col.bounds.center;
                    else
                    {
                        attachTransform.position = go.transform.position;
                        Debug.LogWarning($"{go.name} does not have a collider. Using transform position.");
                    }
                    attachTransform.parent = interactable.transform;
                    if (interactable.attachTransform)
                    {
                        GameObject.DestroyImmediate(interactable.attachTransform.gameObject);
                        j++;
                    }
                    interactable.attachTransform = attachTransform;
                    i++;
                    var scene = SceneManager.GetActiveScene();
                    EditorSceneManager.MarkSceneDirty(scene);
                }
            }
            Debug.Log($"AttachPoints created for {i} gameobjects. Destroyed {j}");
        }

        [MenuItem("Edive/Utils/Fill Interactable Colliders", false, 101)]
        public static void FillColliders()
        {
            Debug.LogError("This wont probably work because the values are not somehow saved after closing the scene...");
            int i = 0;
            int j = 0;
            foreach (var o in Selection.objects)
            {
                var go = (o as GameObject);
                XRGrabInteractable interactable = go?.GetComponent<XRGrabInteractable>();
                if (interactable)
                {
                    Collider[] colliders = go.GetComponentsInChildren<Collider>(true);
                    if (colliders.Length > 0)
                    {
                        j++;
                        if (interactable.colliders.Count > 0)
                        {
                            Debug.Log(go.name + ": There already are some colliders. Overwriting...");
                            interactable.colliders.Clear();
                        }
                        interactable.colliders.AddRange(colliders);
                    }
                    i++;
                    var scene = SceneManager.GetActiveScene();
                    EditorSceneManager.MarkSceneDirty(scene);
                }
            }
            Debug.Log($"Colliders filled for {j} out of {i} interactables.");
        }

        [MenuItem("Edive/Utils/Check Colliders", false, 102)]
        public static void CheckColliders()
        {
            int i = 0;
            foreach (var o in Selection.objects)
            {
                var go = (o as GameObject);
                if (!go.GetComponentInChildren<Collider>())
                {
                    Debug.Log($"{go.name} has no collider");
                    EditorGUIUtility.PingObject(o);
                    i++;
                }
            }
            Debug.Log($"There are {i} objects without colliders");
        }

        private static void Run(UVRN_NetworkManager.ConnectionType connectionType, EdiveSettingsObject.TargetPlatform platform)
        {
            if (!EdiveSettings?.BuildSettings)
            {
                Debug.LogWarning("No build settings");
                return;
            }
            Debug.Log("Using settings from: " + settingsPath);
            EdiveSettings.BuildSettings.connectionType = connectionType;
            EdiveSettings.BuildSettings.targetPlatform = platform;
            //EdiveEditorSettings.lastOpenSceneBeforeRun = EditorApplication.currentScene;
            //EdiveEditorSettings.reloadSceneOnPlayModeStateChange = true;
            OpenScene(EdiveSettings.MainScenePath);
            EditorApplication.isPlaying = true;
        }

        // It worked few times....but somehow mostly does not and there is no time to fix it now
        //private static void EditorApplication_playModeStateChanged(PlayModeStateChange state)
        //{
        //    if (state == PlayModeStateChange.EnteredEditMode && EdiveEditorSettings.reloadSceneOnPlayModeStateChange)
        //    {
        //        Debug.Log("Changing scene back to" + EdiveEditorSettings.lastOpenSceneBeforeRun);
        //        OpenScene(EdiveEditorSettings.lastOpenSceneBeforeRun);
        //        EdiveEditorSettings.reloadSceneOnPlayModeStateChange = false;
        //    }
        //}

        #endregion

        #region Edive/Build

        [MenuItem("Edive/Build/Build Server", false, 211)]
        public static void BuildServerWindows()
        {
            Build.BuildServerWrapper(BuildTarget.StandaloneWindows64);
        }

        [MenuItem("Edive/Build/Build Server (advanced)/Windows x32", false, 212)]
        public static void BuildServerWin64()
        {
            Build.BuildServerWrapper(BuildTarget.StandaloneWindows);
        }

        [MenuItem("Edive/Build/Build Server (advanced)/Windows x64", false, 212)]
        public static void BuildServerWin32()
        {
            Build.BuildServerWrapper(BuildTarget.StandaloneWindows64);
        }

        [MenuItem("Edive/Build/Build Server (advanced)/Linux x64", false, 230)]
        public static void BuildServerLinux64()
        {
            Build.BuildServerWrapper(BuildTarget.StandaloneLinux64);
        }

        /*
        [MenuItem("Edive/Build/Build Server (advanced)/MacOS x64", false, 250)]
        public static void BuildServerMac()
        {
            Build.BuildServerWrapper(BuildTarget.StandaloneOSX);
        }
        */

        [MenuItem("Edive/Build/Build Client", false, 213)]
        public static void BuildClient()
        {
            Build.BuildClientWrapper();
        }

        [MenuItem("Edive/Build/Build All (Windows + Android)", false, 230)]
        public static void BuildAll()
        {
            Build.BuildAllWrapper(BuildTarget.StandaloneWindows64);
        }

        [MenuItem("Edive/Build/Build and run server", false, 231)]
        public static void BuildAndRunServer()
        {
            Build.BuildAndRunServer();
        }

#endregion

        #region Utils

        private static void OpenScene(string scenePath)
        {
            EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
            EditorSceneManager.OpenScene(scenePath);
        }

        public class ProgressBar
        {
            private int maximum;
            private int current;
            private string title;

            public ProgressBar(string title, int steps)
            {
                this.maximum = steps - 1;
                this.title = title;
                this.current = 0;
            }

            ~ProgressBar()
            {
                EditorUtility.ClearProgressBar();
            }

            public void Step(string subtitle)
            {
                EditorUtility.DisplayProgressBar(title, subtitle, this.current / (float)maximum);
                System.Console.WriteLine($"[{title}] {subtitle}");
                this.current++;
                if (this.current > maximum)
                {
                    Thread.Sleep(1000);
                    EditorUtility.ClearProgressBar();
                }
            }
        }

        // Code from https://docs.microsoft.com/en-us/dotnet/standard/io/how-to-copy-directories
        internal static void CopyDir(string source, string destination)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(source);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + source);
            }

            DirectoryInfo[] dirs = dir.GetDirectories();

            // If the destination directory doesn't exist, create it.       
            Directory.CreateDirectory(destination);

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string tempPath = Path.Combine(destination, file.Name);
                file.CopyTo(tempPath, true);
            }

            // Copy subdirs and their contents to new location.
            foreach (DirectoryInfo subdir in dirs)
            {
                string tempPath = Path.Combine(destination, subdir.Name);
                CopyDir(subdir.FullName, tempPath);
            }
        }

        internal static string GetTempLocation()
        {
            string dir = Path.Combine(Path.GetTempPath(), FileUtil.GetUniqueTempPathInProject());
            Directory.CreateDirectory(dir);
            return dir;
        }

        [MenuItem("GameObject/Edive/Copy World Location", false, 0)]
        private static void CopyWorldPosition()
        {
            copiedTransformPosition = Selection.activeTransform.position;
            copiedTransformRotation = Selection.activeTransform.rotation;
        }

        [MenuItem("GameObject/Edive/Paste World Location", false, 1)]
        private static void PasteWorldPosition()
        {
            Selection.activeTransform.position = copiedTransformPosition;
            Selection.activeTransform.rotation = copiedTransformRotation;
        }

        [MenuItem("GameObject/Edive/Copy Components", false, 100)]
        private static void CopyComponents()
        {
            copiedComponents = Selection.activeGameObject.GetComponents<Component>();
            Debug.Log($"Copied {copiedComponents.Length} components");
        }

        [MenuItem("GameObject/Edive/Paste Components", false, 101)]
        private static void PasteComponents()
        {
            if (Selection.gameObjects.Length > 1)
            {
                Debug.LogError("For some random reason you cannot paste components to multiple gameobjects...Not even the coroutine worked");
                return;
            }
            if (copiedComponents == null || copiedComponents.Length < 1) return;

            var fromGO = copiedComponents[0].gameObject;
            GameObject[] targets = new GameObject[Selection.gameObjects.Length];
            Selection.gameObjects.CopyTo(targets, 0);
            EditorCoroutineUtility.StartCoroutine(PasteComponentsCoroutine(copiedComponents, targets), fromGO);
        }

        private static IEnumerator PasteComponentsCoroutine(Component[] components, GameObject[] targetGameObjects)
        {
            foreach (Component copiedComponent in components)
            {
                if (!copiedComponent) continue;
                if (copiedComponent is Transform) continue; // skip Transform

                Debug.Log($"Pasting {copiedComponent.GetType()} from {components[0].gameObject.name}");
                UnityEditorInternal.ComponentUtility.CopyComponent(copiedComponent);
                foreach (var targetGameObject in targetGameObjects)
                {
                    if (targetGameObject)
                    {
                        Debug.Log($"---- to {targetGameObject.name}");
                        UnityEditorInternal.ComponentUtility.PasteComponentAsNew(targetGameObject);
                    }
                    yield return null;
                }
            }
        }
        #endregion
    }
#endif
}
