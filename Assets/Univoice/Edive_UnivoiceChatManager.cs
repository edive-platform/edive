// Created by Vojtech Bruza
using Adrenak.UniVoice;
//using Adrenak.UniVoice.TelepathyNetwork;
using Adrenak.UniVoice.MirrorNetwork;
using Adrenak.UniVoice.UniMicInput;
using System.Linq;
using System;
using UnityEngine;
using UVRN.VoiceChat;
//using static Adrenak.UniVoice.AudioSourceOutput.UniVoiceAudioSourceOutput;
using Adrenak.UniMic;
using Mirror;
using Adrenak.UniVoice.AudioSourceOutput;
using UVRN.Player;
using UnityEngine.SceneManagement;

using System.Collections.Generic;
using Unity.VisualScripting;

namespace Edive.Networking.VoiceChat
{
    public class Edive_UnivoiceChatManager : UVRN_VoiceChatManager
    {
        [SerializeField]
        private ChatroomAgent agent;

        private UniVoiceMirrorNetwork chatroomNetwork;

        [SerializeField]
        GameObject player_avatar_game_object;

        [SerializeField]
        protected UniVoiceMirrorNetwork chatroomNetworkPrefab = null;

        [SerializeField]
        private bool dontDestroyOnLoad = true;

        [SerializeField]
        private int telepathyPort = 5024;

        private bool peersUpdated = false;

        #region Net Manager
        private Edive_NetworkManager m_netManager;
        private Edive_NetworkManager netManager
        {
            get
            {
                if (!m_netManager)
                {
                    m_netManager = (Edive_NetworkManager)Edive_NetworkManager.singleton;
                }
                return m_netManager;
            }
        }
        #endregion

        private void Awake()
        {
            Debug.Log("Microphone devices: " + Microphone.devices.Length);
            Debug.Log("Mic.Instance.Devices.Count: " + Mic.Instance.Devices.Count);
            if (chatroomNetwork == null)
            {
                chatroomNetwork = new UniVoiceMirrorNetwork();
            }
#if UNITY_SERVER
            peersUpdated = true;
            Debug.Log("We are in Server. Creating and empty AudioInput.");
            agent = new ChatroomAgent(chatroomNetwork, new Edive_UnivoiceEmptyAudioInput(), new UniVoiceAudioSourceOutput.Factory());
#else
            Debug.Log("We are in Client. Creating a proper AudioInput.");
            agent = new ChatroomAgent(chatroomNetwork, new UniVoiceUniMicInput(0, 8000, 25), new UniVoiceAudioSourceOutput.Factory());
#endif
        }

        //Flip the Edive_UnivoiceChatManager boolean "peersUpdated" to "false"
        public void changeBool()
        {
            Invoke("changePeersUpdated", 2);
            Invoke("assignLocalAudioSource", 2);
        }

        public void changePeersUpdated()
        {
            peersUpdated = false;
        }
        public void assignLocalAudioSource()
        {
            GameObject localPlayer = GameObject.FindWithTag("Player");
            GameObject audioSourceLocal = GameObject.Find("UniVoice Peer #0");
            audioSourceLocal.transform.SetParent(localPlayer.transform.GetChild(0).GetChild(0));
            audioSourceLocal.transform.position = localPlayer.transform.GetChild(0).GetChild(0).transform.position;
        }
        //Flip the Edive_UnivoiceChatManager boolean "peersUpdated" to "false"

        private void Update()
        {
            if (!peersUpdated)
            {
                var updated = false;
                foreach (var obj in UnityEngine.Object.FindObjectsOfType<UVRN_Player>())
                {
                    if (obj.GetComponent<NetworkIdentity>().netId == 0 || obj.GetComponent<NetworkIdentity>().netId == agent.Network.OwnID)
                    {
                        continue;
                    }

                    if (obj.GetComponent<IAudioOutput>() == null)
                    {
                        foreach (KeyValuePair<short, IAudioOutput> kvp in agent.PeerOutputs)
                        {
                            if (kvp.Key == obj.GetComponent<NetworkIdentity>().netId)
                            {
                                ((UniVoiceAudioSourceOutput)kvp.Value).AudioSource.transform.parent = obj.transform.GetChild(0).GetChild(0);
                                ((UniVoiceAudioSourceOutput)kvp.Value).AudioSource.spatialBlend = 1;
                                updated = true;
                                break;
                            }
                        }
                    }
                }
                if (updated)
                {
                    peersUpdated = true;
                }
            }
        }
        
        public override bool IsMicMuted()
        {
            //bool micMuted = netManager.VoiceChatManager.IsMicMuted();
            //there must be something in the univoice code that will tell us this
            //return false;

            return agent.MuteSelf;
        }

        public bool IsSpeaking(int iD)
        {
            //return ((UniVoiceTelepathyNetwork)agent.Network).IsPeerSpeaking(iD);
            return false;
        }

        public override void SetMicMuted(bool value)
        {
			Debug.Log($"Setting Mic Muted to {value}.");
			agent.MuteSelf = value;
			Debug.Log($"Mic Muted is now {agent.MuteSelf}.");
		}

        public override void SetMicVolume(int value)
        {
            //agent.AudioInput.Mic
        }

        public override void SetVoiceChatVolume(int value)
        {
        }

        public override void SetWorldPosition(Vector3 position, Vector3 forward, Vector3 up)
        {
            // is this necessary? We assign the audio output to the avatar directly
            //univoice doesn't transmit position
            //get this from Mirror and the game object itself
        }

        
        public override void StartVoiceChat(string id)
        {
            Debug.Log($"STARTVOICECHAT id: {id}");

            agent.Network.OnPeerJoinedChatroom += id =>
            {
                // This gets executed before the UVRN_Player object is created.
                // Just set this variable so we know the list has to be updated. 
                peersUpdated = false;
            };
        }


        [Client]
        public override void StopVoiceChat()
        {
            // TODO client side
            if(agent.CurrentMode == ChatroomAgentMode.Guest)
            {
				agent.Network.LeaveChatroom();
			}

            // TODO server side
            if(agent.CurrentMode == ChatroomAgentMode.Host)
            {
				agent.Network.CloseChatroom();
			}
        }
        

        public override void ToggleMuteOtherUser(string iD)
        {
            short s;
            if (!short.TryParse(iD, out s))
            {
                return;
            }
            if (agent.PeerSettings.TryGetValue(s, out  var value))
            {
                value.muteThem = true;
            }
        }
    }
}
