This folder contains all the currently downloaded environments.
To download any environment you have to use the "Edive/Setup Window" menu.

This folder's content is ignored by git, so you have to control the versioning for each environment separately.