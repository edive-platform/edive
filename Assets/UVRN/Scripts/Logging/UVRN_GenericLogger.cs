// Created by Vladimir Zbanek, Vojtech Bruza
using UnityEngine;
using System.Collections;
using Mirror;

namespace UVRN.Logging
{
    
    /// <summary>
    /// Logger documentation:
    /// https://docs.google.com/document/d/18KSbT1rmckpMeMElpYXJn3fVAKfFqs1xG05pl6pkzCs/edit?usp=sharing
    /// </summary>
    
    [DisallowMultipleComponent]
    public class UVRN_GenericLogger : MonoBehaviour
    {

        public string ObjectID { get; protected set; }

        public string ObjectType { get; protected set; }

        [Tooltip("How many times per second will the logger log changes in transforms."), Range(0, 60), Delayed]
        public float logFrequency = 24f;

        protected IEnumerator logRoutine;

        #region what to log
        [Header("What to log:")]
        public bool logPosition = true;
        public bool logRotation = true;
        public bool logLocalScale = true;

        [Tooltip("Log other, custom events.")]
        public bool logCustomEvents = true;
        #endregion

        /// <summary>
        /// Override this method to write custom initialization.
        /// </summary>
        public virtual void Init()
        {
            Init(null, "Generic Object", true);
        }

        protected string GetDefaultID()
        {
            // TODO use some better default ID
            return $"{gameObject.name}, instance: {gameObject.GetInstanceID()}";
        }

        /// <summary>
        /// Initialize values and setup what is needed for the logger to work.
        /// </summary>
        /// <param name="logOnStart">Start logging immediately on start. If false, logger has to be start by StartLogging() method.</param>
        /// <param name="objectType">What kind of object is this logger attached to, will be displayed in the log file.</param>
        /// <param name="objectID">Custom identifier of parent object. 
        /// By default, the ID is the object name + instance ID. Will be displayed in the log file.</param>
        public void Init(string objectID, string objectType, bool logOnStart)
        {
            ObjectID = string.IsNullOrEmpty(objectID) ? GetDefaultID() : objectID;
            ObjectType = objectType;

            Debug.Log("Init logger for " + ObjectID);
            logRoutine = LogCoroutine();
            if (logOnStart) StartLoggingRoutine();
        }

        /// <summary>
        /// Start/resume the logging routine.
        /// </summary>
        public virtual void StartLoggingRoutine()
        {
            if (gameObject.activeInHierarchy) StartCoroutine(logRoutine);
        }

        /// <summary>
        /// Stop/pause the logging routine.
        /// </summary>
        public virtual void StopLoggingRoutine()
        {
            StopCoroutine(logRoutine);
        }

        /// <summary>
        /// The logging routine. Logs position, rotation and scale of parent object.
        /// </summary>
        protected virtual IEnumerator LogCoroutine()
        {
            if (logFrequency > 0)
            {
                while (true)
                {
                    LogPosition();
                    LogRotation();
                    LogScale();
                    yield return new WaitForSecondsRealtime(1.0f / logFrequency);
                }
            }
        }

        /// <summary>
        /// Logs the position of parent object (if position logging is enabled).
        /// </summary>
        [Server]
        protected void LogPosition()
        {
            if (logPosition)
                UVRN_Logger.Instance.LogPosition(this, transform.position);
        }

        /// <summary>
        /// Short for UVRN_Logger.Instance.LogRotation(...).
        /// Logs the rotation of parent object  (if rotation logging is enabled).
        /// </summary>
        [Server]
        protected void LogRotation()
        {
            if (logRotation)
                UVRN_Logger.Instance.LogRotation(this, transform.rotation);
        }

        /// <summary>
        /// Short for UVRN_Logger.Instance.LogLocalScale(...).
        /// Logs the local scale of parent object (if scale logging is enabled).
        /// </summary>
        [Server]
        protected void LogScale()
        {
            if (logLocalScale)
                UVRN_Logger.Instance.LogLocalScale(this, transform.localScale);
        }

        /// <summary>
        /// Short for UVRN_Logger.Instance.LogCustomMessage(...).
        /// Log a custom message (if custom event logging is enabled). Message parts will be separated into columns.
        /// </summary>
        /// <param name="message">Messages to log, parts will be separated into columns.</param>
        [Server]
        protected void LogCustomMessage(params string[] message)
        {
            if (logCustomEvents)
                UVRN_Logger.Instance.LogCustomMessage(this, message);
        }

        /// <summary>
        /// Short for UVRN_Logger.Instance.LogCustomMessage(...).
        /// Log a custom message. Message parts will be separated into columns.
        /// Will not check if custom event logging is enabled.
        /// </summary>
        /// <param name="message"></param>
        [Server]
        protected void ForceLogCustomMessage(params string[] message)
        {
            UVRN_Logger.Instance.LogCustomMessage(this, message);
        }

        /// <summary>
        /// Short for UVRN_Logger.Instance.GetNameFromConnection(...).
        /// Puts together a player identifier from NetworkConnection, use for human-readable logging.
        /// </summary>
        /// <param name="conn">Player's network connection</param>
        /// <returns>Player identifier string.</returns>
        protected static string GetNameFromConnection(NetworkConnection conn)
        {
            return UVRN_Logger.GetNameFromConnection(conn);
        }
    }
}