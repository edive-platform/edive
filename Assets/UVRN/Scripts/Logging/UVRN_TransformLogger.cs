// Created by Vojtech Bruza
using System;
using System.Collections;
using UnityEngine;
using UVRN.Player;

namespace UVRN.Logging
{
    [Obsolete("Use the newer system (this class is not updated to it) - check e.g. the new player logger that inherits from the generic one")]
    public class UVRN_TransformLogger : MonoBehaviour
    {
        private UVRN_Player ownerPlayer;
        private Transform transformToLog;
        private IEnumerator logCoroutine;

        /// <summary>
        /// Controls if the logger should run (will not run if disabled)
        /// </summary>
        private bool m_shouldLog;

        /// <summary>
        /// Controls if the logger actually runs = logs
        /// </summary>
        private bool m_logging;

        private void SetShouldLog(bool value)
        {
            m_shouldLog = value;
            SetLogging(m_shouldLog);
        }

        private void SetLogging(bool value)
        {
            if (m_shouldLog)
            {
                m_logging = value;
                if (gameObject.activeInHierarchy)
                {
                    if (m_logging) StartCoroutine(logCoroutine);
                    else StopCoroutine(logCoroutine);
                }
            }
            else
            {
                Debug.LogError("Cannot start logging when logging is not allowed.");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="owner">Owner player</param>
        /// <param name="transform">Transform to log</param>
        /// <param name="frequency">How many times per second are the values logged.</param>
        /// <param name="startLogger">If the logger should start immediatelly.</param>
        public void Init(UVRN_Player owner, Transform transform, float frequency, bool startLogger)
        {
            ownerPlayer = owner;
            transformToLog = transform;
            logCoroutine = LogCoroutine(frequency);
            if (startLogger) SetShouldLog(true);
        }

        private void OnEnable()
        {
            if (m_shouldLog) SetLogging(true);
        }

        private void OnDisable()
        {
            SetLogging(false);
        }

        /// <summary>
        /// Call this to start logging.
        /// </summary>
        public void StartLogging()
        {
            SetShouldLog(true);
        }

        public void StopLogging()
        {
            SetShouldLog(false);
        }

        private IEnumerator LogCoroutine(float frequency)
        {
            if (frequency > 0)
            {
                while (m_shouldLog) { // can be while true...because the coroutine is anyway stopped
                    yield return new WaitForSecondsRealtime(1.0f / frequency);
                    UVRN_Logger.Instance.LogEntry(ownerPlayer.ID.ToString(), transformToLog, "gameobject: " + transformToLog.gameObject.name);
                }
            }
        }
    }
}
