﻿// Created by Vojtech Bruza
using UnityEngine;

namespace UVRN.Logging.Debugging
{
    public class UVRN_UIDebugLogger : MonoBehaviour
    {
        [SerializeField]
        private int removeLinesOlderThan = 7;

        public string debugText;

        [SerializeField]
        private UVRN_DebugText debugTextUI;

        private void Start()
        {
            debugTextUI.SetText("");
            Application.logMessageReceived += OnLogMessageReveived;
            debugTextUI.manager = this;
        }

        private void OnLogMessageReveived(string logString, string stackTrace, LogType type)
        {
            // GUI can be destroyed before other object at end of application
            if (debugTextUI == null)
            {
                return;
            }

            string newMessage = logString;

            switch (type)
            {
                case LogType.Error:
                    newMessage = "<color=red>" + logString + "</color>";
                    break;
                case LogType.Warning:
                    newMessage = "<color=yellow>" + logString + "</color>";
                    break;
                case LogType.Exception:
                    newMessage = "<color=purple>" + logString + "</color>";
                    break;
                default:
                    newMessage = logString;
                    break;
            }

            if (debugText == "")
            {
                debugText = newMessage;
            }
            else
            {
                debugText = debugText + "\n" + newMessage;
            }

            int lineCount = debugText.Split('\n').Length - 1;
            int index = debugText.IndexOf('\n') + 1;

            while (lineCount > removeLinesOlderThan)
            {
                // remove too old lines
                debugText = debugText.Substring(index);
                lineCount = debugText.Split('\n').Length - 1;
                index = debugText.IndexOf('\n') + 1;
            }

            debugTextUI.SetText(debugText);
        }

    }
}
