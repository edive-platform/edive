// Created by Vojtech Bruza
using TMPro;
using UnityEngine;

namespace UVRN.Logging.Debugging
{
    public class UVRN_DebugText : MonoBehaviour
    {
        public UVRN_UIDebugLogger manager;

        [SerializeField]
        private TextMeshProUGUI debugTextUI;


        // Changed from OnEnable, because it was being called before the initialization of the manager component and hence throwing a "reference not set to instance" type of error. (O. Kvarda)
        void Start()
        {
            SetText(manager.debugText);
        }

        public void SetText(string text)
        {
            debugTextUI.SetText(text);
        }
    }
}
