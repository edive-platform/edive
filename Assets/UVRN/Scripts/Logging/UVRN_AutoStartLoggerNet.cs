// Created by Vladimir Zbanek, Vojtech Bruza
using Mirror;
using UnityEngine;

namespace UVRN.Logging
{
    /// <summary>
    /// This class can be used to auto-init a logger for a client/server.
    /// The logging process will not start if the init method in the logger class itself is not set to start it.
    /// </summary>
    public class UVRN_AutoStartLoggerNet : NetworkBehaviour
    {
        [SerializeField]
        [Tooltip("Logger to init")]
        private UVRN_GenericLogger logger = null;
        [SerializeField]
        [Tooltip("Should the logger be initialized for a server.")]
        private bool logOnServer = true;
        [SerializeField]
        [Tooltip("Should the logger be initialized for a client.")]
        private bool logOnClient = false;

        private void Reset()
        {
            logger = GetComponent<UVRN_GenericLogger>();
        }

        public override void OnStartServer()
        {
            if (logOnServer) logger.Init();
        }

        public override void OnStartClient()
        {
            if (logOnClient) logger.Init();
        }
    }
}
