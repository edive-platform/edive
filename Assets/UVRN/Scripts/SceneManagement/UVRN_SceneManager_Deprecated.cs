﻿// Created by Vojtech Bruza
using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UVRN.Helpers;

namespace UVRN.SceneManagement
{
    /// <summary>
    /// Scene Manager for loading and unloading scenes on the server and clients.
    /// </summary>
    public class UVRN_SceneManager_Deprecated : Singleton<UVRN_SceneManager_Deprecated>
    {
        [SerializeField]
        private UVRN_NetworkManager uvrn_NetworkManager;

        /// <summary>
        /// Network sync stuff. Shared scene with the Network Manager.
        /// </summary>
        [Scene]
        public string mainSharedOnlineScene;

        /// <summary>
        /// Client specific stuff.
        /// </summary>
        [Scene]
        public string clientOnlyOfflineScene;

        /// <summary>
        /// Server specific stuff.
        /// </summary>
        [Scene]
        public string serverOnlyOfflineScene;

        /// <summary>
        /// Room = online additive scene.
        /// One Client can have multiple rooms loaded (larger maps), but one is prefered.
        /// If two clients are in one room, they should see each other (otherwise not).
        /// TODO Define rooms composed of multiple rooms somewhere. Scriptable objects probably.
        /// </summary>
        [Scene]
        public string[] allRooms;

        // TODO do not load all rooms on server. Instead do it on demand only - if a client wants create a new scene.
        // Load the scene on the server and then add it to this set. (TODO multiple same scenes?)
        /// <summary>
        /// On server: All scenes loaded on the server. Loads at server startup.
        /// On client: All currently loaded scenes (usually just one).
        /// </summary>
        private HashSet<string> loadedRooms = new HashSet<string>();

        public UnityEvent OnRoomLoaded = new UnityEvent();


        #region MonoBehaviour Methods

        private void OnEnable()
        {
            uvrn_NetworkManager.Server_OnStart.AddListener(Server_OnStart);
            uvrn_NetworkManager.Server_OnStop.AddListener(Server_OnStop);

            uvrn_NetworkManager.Client_OnStart.AddListener(Client_OnStart);
            uvrn_NetworkManager.Client_OnStop.AddListener(Client_OnStop);
            uvrn_NetworkManager.Client_OnDisconnectFromServer.AddListener(Client_OnDisconnectFromServer);
        }

        private void OnDisable()
        {
            uvrn_NetworkManager.Server_OnStart.RemoveListener(Server_OnStart);
            uvrn_NetworkManager.Server_OnStop.RemoveListener(Server_OnStop);

            uvrn_NetworkManager.Client_OnStart.RemoveListener(Client_OnStart);
            uvrn_NetworkManager.Client_OnStop.RemoveListener(Client_OnStop);
            uvrn_NetworkManager.Client_OnDisconnectFromServer.RemoveListener(Client_OnDisconnectFromServer);
        }

        private void FixedUpdate()
        {
            // Get the all physics scenes
            foreach (string sceneName in allRooms)
            {
                Scene scene = SceneManager.GetSceneByName(sceneName);
                if (scene.IsValid() && scene.isLoaded)
                {
                    PhysicsScene physicsScene = scene.GetPhysicsScene();
                    if (!physicsScene.IsValid())
                        return; // do nothing if the physics Scene is not valid.

                    // Note that generally, we don't want to pass variable delta to Simulate as that leads to unstable results.
                    physicsScene.Simulate(Time.fixedDeltaTime);
                    // Here you can access the transforms state right after the simulation, if needed...
                }
            }
        }
        #endregion

        #region Server

        private void Server_OnStart()
        {
            StartCoroutine(StartServerCoroutine());
        }

        private IEnumerator StartServerCoroutine()
        {
            yield return LoadSceneAdditiveCoroutine(serverOnlyOfflineScene);
            StartCoroutine(LoadAllRoomsCoroutine());
        }

        private void Server_OnStop()
        {
            // TODO tell clients to unload all scenes
            StartCoroutine(UnloadAllRoomsCoroutine());
        }

        #endregion

        #region Client

        private void Client_OnStart()
        {
            StartCoroutine(StartClientCoroutine());
        }

        private IEnumerator StartClientCoroutine()
        {
            yield return LoadSceneAdditiveCoroutine(clientOnlyOfflineScene);

            // TODO add a lobby room to load on joining a server
            //StartCoroutine(ChangeRoomToCoroutine(lobby));
            StartCoroutine(ChangeRoomToCoroutine(allRooms[0]));
        }

        private void Client_OnDisconnectFromServer()
        {
            // go back from lobby
            StartCoroutine(UnloadRoomCoroutine(allRooms[0]));

            // called when when failed to connect to server
        }

        private void Client_OnStop()
        {
            //StartCoroutine(UnloadAllRoomsCoroutine());
        }

        #endregion

        #region Scene Loading Methods

        /// <summary>
        /// Unloads all loaded rooms and loads instead only the given room.
        /// </summary>
        /// <param name="roomName">Room name to change to.</param>
        /// <returns></returns>
        private IEnumerator ChangeRoomToCoroutine(string roomName)
        {
            yield return UnloadAllRoomsCoroutine();
            yield return LoadRoomCoroutine(roomName);
        }

        /// <summary>
        /// Same as <see cref="ChangeRoomToCoroutine(string)"/>, but loads multiple rooms instead of just one.
        /// </summary>
        /// <param name="roomNames"></param>
        /// <returns></returns>
        private IEnumerator ChangeRoomToCoroutine(IEnumerable<string> roomNames)
        {
            yield return UnloadAllRoomsCoroutine();
            foreach (var roomName in roomNames)
            {
                yield return LoadRoomCoroutine(roomName);
            }
        }
        /// <summary>
        /// Load (enter) a given room.
        /// Room must be loaded on the server first, to be able to load it on client.
        /// </summary>
        /// <param name="sceneName"></param>
        /// <returns></returns>
        private IEnumerator LoadRoomCoroutine(string sceneName)
        {
            // TODO what happens when I try to load it on client, but server does not.
            Debug.Log($"Loading room {sceneName}");
            var op = SceneManager.LoadSceneAsync(sceneName, new LoadSceneParameters(LoadSceneMode.Additive, LocalPhysicsMode.Physics3D));
            yield return op;
            loadedRooms.Add(sceneName);
            OnRoomLoaded.Invoke();
            Debug.Log($"Loaded room {sceneName}");
        }

        private IEnumerator UnloadRoomCoroutine(string sceneName, bool unloadAssets = true)
        {
            Debug.Log("Unloading room: " + sceneName);

            if (SceneManager.GetSceneByName(sceneName).IsValid() || SceneManager.GetSceneByPath(sceneName).IsValid())
            {
                yield return SceneManager.UnloadSceneAsync(sceneName);
                Debug.Log($"Unloaded room {sceneName}");
            }

            if (unloadAssets) yield return Resources.UnloadUnusedAssets();
        }

        private IEnumerator LoadAllRoomsCoroutine()
        {
            Debug.Log("Loading all rooms");

            foreach (string sceneName in allRooms)
            {
                yield return LoadRoomCoroutine(sceneName);
            }
        }

        private IEnumerator UnloadAllRoomsCoroutine()
        {
            Debug.Log("Unloading rooms");

            foreach (string sceneName in loadedRooms)
            {
                yield return UnloadRoomCoroutine(sceneName, false);
            }
            yield return Resources.UnloadUnusedAssets();
        }

        private IEnumerator LoadSceneAdditiveCoroutine(string sceneName)
        {
            Debug.Log($"Loading additive scene: {sceneName}");
            var op = SceneManager.LoadSceneAsync(sceneName, new LoadSceneParameters(LoadSceneMode.Additive));
            yield return op;
            Debug.Log($"Scene loaded: {sceneName}");
        }


        //private void MsgClientToLoadScene(NetworkConnection conn, string roomName)
        //{
        //    Not sure if works...but something like this should. Maybe use TargetRPC instead?
        //    NetworkServer.SendToClientOfPlayer(conn.identity, new SceneMessage { sceneName = roomName, sceneOperation = SceneOperation.LoadAdditive });
        //}

        //// TODO something like this on the client (https://mirror-networking.com/docs/Articles/Components/NetworkSceneChecker.html?q=scene)
        //public void LoadScene(string subScene)
        //{
        //    // Loading the subscene(s) on the server is through the normal process with SceneManager:
        //    // SceneManager.LoadSceneAsync(subScene, LoadSceneMode.Additive);

        //    // Next, you will send a SceneMessage to the client telling it to load a subscene additively:

        //    SceneMessage msg = new SceneMessage
        //    {
        //        sceneName = subScene,
        //        sceneOperation = SceneOperation.LoadAdditive
        //    };

        //    connectionToClient.Send(msg);


        //    // Then, on the server only, you just move the player object to the subscene:

        //    // Position the player object in world space first
        //    // This assumes it has a NetworkTransform component that will update clients
        //    player.transform.position = new Vector3(100, 1, 100);

        //    // Then move the player object to the subscene
        //    SceneManager.MoveGameObjectToScene(player, subScene);
        //}
        #endregion
    }
}
