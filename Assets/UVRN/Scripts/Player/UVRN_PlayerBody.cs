﻿// Created by Vojtech Bruza
using UnityEngine;

namespace UVRN.Player
{
    public class UVRN_PlayerBody : MonoBehaviour
    {
        void LateUpdate()
        {
            transform.eulerAngles = new Vector3(0, transform.parent.eulerAngles.y, 0);
        }
    }
}
