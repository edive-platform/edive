// Created by Vojtech Bruza
using System;
using UnityEngine;

namespace UVRN
{
    public class UVRN_LinePointer : MonoBehaviour
    {
        public float raycastLength = 10f;

        public Transform reticleTransform;

        public LineRenderer lineRenderer;
        public bool showReticle;

        public Gradient validColorGradient;
        public Gradient invalidColorGradient;

        public bool ValidActive { get; private set; }

        public LayerMask layerMask = -1;

        public void SetColor(Color color)
        {
            for (int i = 0; i < validColorGradient.colorKeys.Length; i++)
            {
                validColorGradient.colorKeys[i].color = color;
            }
            for (int i = 0; i < invalidColorGradient.colorKeys.Length; i++)
            {
                invalidColorGradient.colorKeys[i].color = color;
            }
        }

        private void Start()
        {
            SetInvalidVisual();
        }

        private void Update()
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, raycastLength, layerMask: layerMask))
            {
                ValidActive = true;
                if (showReticle && reticleTransform)
                {
                    reticleTransform.position = hit.point;
                    if (!reticleTransform.gameObject.activeSelf) reticleTransform.gameObject.SetActive(true);
                }
                if (lineRenderer)
                {
                    Vector3[] linePoints = { Vector3.zero,
                        new Vector3(0, 0, Vector3.Distance(transform.position, hit.point)) };
                    lineRenderer.positionCount = linePoints.Length;
                    lineRenderer.SetPositions(linePoints);
                    //TODO nice: valid color based on player color?
                    lineRenderer.colorGradient = validColorGradient;
                }
            }
            else if (ValidActive)
            {
                // TODO add timer to prevent flickering?
                SetInvalidVisual();
            }
        }

        private void SetInvalidVisual()
        {
            if (showReticle && reticleTransform) reticleTransform.gameObject.SetActive(false);
            if (lineRenderer)
            {
                lineRenderer.SetPositions(new[] { Vector3.zero, new Vector3(0, 0, raycastLength) });
                lineRenderer.colorGradient = invalidColorGradient;
            }
            ValidActive = false;
        }
    }
}
