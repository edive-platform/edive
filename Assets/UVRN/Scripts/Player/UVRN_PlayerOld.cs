﻿// Created by Vojtech Bruza
using Mirror;
using UnityEngine;
using UVRN.Helpers;

namespace UVRN.Player
{
    public class UVRN_PlayerOld : NetworkBehaviour
    {
        private UVRN_NetworkManager m_netManager;
        private UVRN_NetworkManager uvrn_NetworkManager
        {
            get
            {
                if (!m_netManager)
                {
                    m_netManager = (UVRN_NetworkManager)UVRN_NetworkManager.singleton;
                }
                return m_netManager;
            }
        }

        private UVRN_XRManager m_uvrn_XRManager;
        private UVRN_XRManager uvrn_XRManager
        {
            get
            {
                if (!m_uvrn_XRManager)
                {
                    m_uvrn_XRManager = (UVRN_XRManager) UVRN_XRManager.Instance;
                }
                if (m_uvrn_XRManager != UVRN_XRManager.Instance)
                {
                    Debug.LogError("XR instance changed.");
                }
                return m_uvrn_XRManager;
            }
        }

        [SerializeField]
        private UVRN_PlayerModel playerModel;

        [SerializeField]
        private TextMesh playerNameText;

        [SerializeField]
        private Transform floatingName;

        //[SerializeField]
        //private Transform speakingVisualization;

        [SerializeField]
        private Material playerMaterial;

        [SyncVar(hook = nameof(OnNameChanged))]
        public string PlayerID;

        [SyncVar(hook = nameof(OnColorChanged))]
        public Color PlayerColor = Color.white;

        [SerializeField]
        private string localLayerString = "LocalInvisible";

        [SerializeField]
        private string otherPlayerLayerString = "Default";

        //[SyncVar(hook = nameof(OnSpeakingChanged))]
        //public bool Speaking = false;

        //private void OnSpeakingChanged(bool _Old, bool _New)
        //{
        //    speakingVisualization.gameObject.SetActive(_New);
        //}

        public void OnNameChanged(string _Old, string _New)
        {
            playerNameText.text = "User_" + PlayerID;
        }

        public void OnColorChanged(Color _Old, Color _New)
        {
            playerNameText.color = _New;
            playerModel.Color = _New;
        }

        public override void OnStartLocalPlayer()
        {
            if (isLocalPlayer) // needed?
            {
                Debug.Log("Init local player.");
                InitPlayerProfile();
            }
        }

        private void InitPlayerProfile()
        {
            CmdSetupPlayer();
        }

        private void InitPlayerModelTracking()
        {
            // set spawned models to track instantiated XR controllers and player representation
            playerModel.headModel.movementSourceToTrack = uvrn_XRManager.GetHead();
            playerModel.leftHandModel.movementSourceToTrack = uvrn_XRManager.GetLeftHand();
            playerModel.rightHandModel.movementSourceToTrack = uvrn_XRManager.GetRightHand();
            // hide the player body/head
            playerModel.headModel.transform.ChangeLayersRecursively(localLayerString);
            // hide local laser pointer duplicates
            playerModel.leftHandModel.gameObject.GetComponentInChildren<LineRenderer>().enabled = false;
            playerModel.rightHandModel.gameObject.GetComponentInChildren<LineRenderer>().enabled = false;
            // TODO make this smarter, so the user know he needs this layer
        }

        [Command]
        public void CmdSetupPlayer()
        {
            // player info sent to server, then server updates sync vars which handles it on all clients
            //PlayerID = connectionToClient.identity.netId.ToString();
            //PlayerColor = Color.HSVToRGB(UnityEngine.Random.Range(0f, 1f), .75f, .75f);

            //playerNameText.text = "User_" + PlayerID;

            RpcSetupPlayer(PlayerID, PlayerColor);
        }

        [ClientRpc]
        private void RpcSetupPlayer(string userID, Color color)
        {

            if (isLocalPlayer)
            {
                InitPlayerModelTracking();
                Debug.Log("My user ID: " + userID);
            }
            else
            {
                Debug.Log("Init other player.");
                playerModel.headModel.transform.ChangeLayersRecursively(otherPlayerLayerString);
            }

        }

        private void Update()
        {
            if (!isLocalPlayer)
            {
                // turn all other player names to look at the local camera
                floatingName.LookAt(Camera.main?.transform);
                return;
            }
        }
    }
}
