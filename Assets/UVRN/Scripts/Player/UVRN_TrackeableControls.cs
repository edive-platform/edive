// Created by Vojtech Bruza
using UnityEngine;

/// <summary>
/// Dont forget to implement Instance getter.
/// Using abstract class because C# interfaces cannot use static methods :(
/// </summary>
namespace UVRN
{
    public abstract class UVRN_TrackeableControls : MonoBehaviour
    {
        [Tooltip("Will use this transform if none set. Needed to ensure we have only one audioListener in the scene.")]
        [SerializeField]
        private Transform audioListenerParent;

        public static UVRN_TrackeableControls Instance { get; protected set; }
        public abstract Transform GetHead();
        public abstract Transform GetLeftHand();
        public abstract Transform GetRightHand();

        protected virtual void OnEnable()
        {
            if (!Instance) Instance = this;
            else Debug.LogError($"Creating new {nameof(UVRN_TrackeableControls)} when one already exists {nameof(this.GetType)}.");
        }

        protected virtual void OnDisable()
        {
            Instance = null;
        }

        public void SetAudioListener(AudioListener audioListener)
        {
            if (audioListenerParent == null)
            {
                audioListener.transform.parent = transform;
            }
            else audioListener.transform.parent = audioListenerParent;
        }
    }
}
