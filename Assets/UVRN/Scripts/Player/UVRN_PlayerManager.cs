// Created by Vojtech Bruza
using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UVRN.Logging;
using UVRN.VoiceChat;

namespace UVRN.Player
{
    /// <summary>
    /// This class should manage all stuff related to the players and their connections establishing.
    /// But right now it is kind of overlapping with the functionallity in the UVRN_Player class
    /// so it could use a bit of refactor and cleanup.
    /// </summary>
    public class UVRN_PlayerManager : MonoBehaviour
    {
        // TODO use Mirror's default basic authenticator?
        [Serializable]
        public class RolePasswordPair
        {
            public string role;
            public string password;
        }

        public UVRN_NetworkManager NetworkManager;

        // TODO dont use just simple strings but serialize the roles to SO to be able to display them in the Editor
        [Tooltip("All allowed roles for this server. If the password is empty, no password is required. If this is list empty, allow everyone.")]
        public List<RolePasswordPair> rolePasswordPairs = new List<RolePasswordPair>() { };


        protected bool requireRolePassword = false;

        public uint LocalPlayerID { get => NetworkClient.localPlayer.netId; }

        // Set from the NetworkManager (in Start)...that way all clients and server knows what to spawn (is registered as spawnable)
        [HideInInspector]
        public GameObject PlayerPrefab;

        public UnityEventString Client_OnInvalidLogin = new UnityEventString();

        // TODO remove this in the future and use the Mirror auth instead
        // this is just to be able to show the message in the lobby when disconnected from the server due to invalid login
        // this class is not destroyed therefore the message gets transfered
        public string failedAuthMessage { get; private set; }

        public static Dictionary<uint, UVRN_Player> Client_ConnectedPlayers => UVRN_Player.Client_ConnectedPlayers;

        public void Awake()
        {
            PlayerPrefab = NetworkManager.playerPrefab;

            Debug.Log("Available mic devices: " + Microphone.devices.Length);
        }

        private void Start()
        {
            // postponed to start since voice chat might not be instantiated before that
            if (NetworkManager.VoiceChatManager != null) NetworkManager.VoiceChatManager.OnLocalMute.AddListener(Client_Voice_OnLocalPlayerMute);
            else Debug.LogError("Could not register voice chat mute. Missing voice chat manager.");
        }

        private void OnDestroy()
        {
            // moved from on disable since the counterpart is moved to start
            if (NetworkManager.VoiceChatManager != null) NetworkManager.VoiceChatManager.OnLocalMute.RemoveListener(Client_Voice_OnLocalPlayerMute);
        }

        private void OnEnable()
        {
            NetworkManager.Server_OnStart.AddListener(Server_OnStart);
            NetworkManager.Client_OnStart.AddListener(Client_OnStart);
            NetworkManager.Client_OnSceneChanged.AddListener(Client_OnSceneChanged);
            NetworkManager.Server_OnClientDisconnecting.AddListener(Server_RemovePlayer);

            NetworkManager.Client_OnStop.AddListener(Client_OnStop);
        }

        private void OnDisable()
        {
            NetworkManager.Server_OnStart.RemoveListener(Server_OnStart);
            NetworkManager.Client_OnStart.RemoveListener(Client_OnStart);
            NetworkManager.Client_OnSceneChanged.RemoveListener(Client_OnSceneChanged);
            NetworkManager.Server_OnClientDisconnecting.RemoveListener(Server_RemovePlayer);
            
            NetworkManager.Client_OnStop.RemoveListener(Client_OnStop);
        }

        public static bool TryGetPlayer(uint id, out UVRN_Player player)
        {
            Client_ConnectedPlayers.TryGetValue(id, out player);
            return player != null;
        }

        [Client]
        private void Client_Voice_OnLocalPlayerMute(string idString, bool muted)
        {
            uint id = uint.Parse(idString);
            
            // you cannot mute yourself
            if (id == LocalPlayerID) return;

            UVRN_Player.Client_ConnectedPlayers.TryGetValue(id, out var playerToMute);
            // mute the player only when not already muted, otherwise Vivox was somehow spamming the unmuting
            if (playerToMute && playerToMute.Muted != muted)
            {
                playerToMute.Mute(muted);
                var un = muted ? "" : "un";
                SendInteractionNotificationToServer(id, $"{un}muted");
            }
        }

        [Server]
        private void Server_RemovePlayer(NetworkConnection conn)
        {
            // conn.identity should be the player object
            var player = conn.identity?.GetComponent<UVRN_Player>();
            if (!player)
            {
                Debug.LogError("Non-critical: Could not remove null player");
                return;
            }
            // can be null because sometimes the player can be destroyed before this is called?
            var playerID = player?.ID;
            // Logger can be null if the server is shut before all players disconnect
            UVRN_Logger.Instance?.LogEntry("Server", $"player left; id: {playerID};");
        }

        [Server]
        public void Server_OnStart()
        {
            NetworkServer.RegisterHandler<PlayerCreationRequestMessage>(Server_OnPlayerCreationRequest);
            NetworkServer.RegisterHandler<PlayerInteractionMessage>(Server_OnPlayerInteractionMessage);
            NetworkServer.RegisterHandler<PlayerActionMessage>(Server_OnPlayerActionMessage);
            NetworkServer.RegisterHandler<PlayerLeavingMessage>(Server_OnPlayerLeavingMessage);
        }

        [Client]
        public void Client_OnStart()
        {
            NetworkClient.RegisterHandler<PlayerCreationFailedResponseMessage>(Client_OnPlayerCreationFailedResponse);
            NetworkClient.RegisterHandler<PlayerLeftMessage>(Client_OnPlayerLeftMessage);
            // clear the message when connecting again
            failedAuthMessage = "";
        }

        [Server]
        private void Server_OnPlayerCreationRequest(NetworkConnectionToClient conn, PlayerCreationRequestMessage request)
        {
            if (requireRolePassword && !ValidateProfile(request.profile))
            {
                Debug.Log("Player does not have permission to join.");
                var invalidLoginResponse = new PlayerCreationFailedResponseMessage()
                {
                    errorText = "Invalid login credentials"
                };
                conn.Send(invalidLoginResponse);
                // TODO these messages do not get sent because the player is disconnected before it recieves the reason
                // the coroutine is just a temporary fix
                StartCoroutine(DisconnectAfter(conn, 1));
                return;
            }

            InstantiatePlayer(conn, request.profile);
        }

        private IEnumerator DisconnectAfter(NetworkConnection conn, float seconds)
        {
            yield return new WaitForSeconds(seconds);
            conn.Disconnect();
        }

        [Server]
        private bool ValidateProfile(UVRN_PlayerProfile profile)
        {
            // if there are no roles defined, let everyone
            if (rolePasswordPairs.Count == 0) return true;
            foreach(var r in rolePasswordPairs)
            {
                // if there is the role with no password or with the same password
                if (r.role == profile.role && (r.password == "" || r.password == profile.password)) return true;
            }
            return false;
        }

        [Server]
        private void InstantiatePlayer(NetworkConnectionToClient conn, UVRN_PlayerProfile profile)
        {
            // The scene should be already loaded on the client

            var go = Instantiate(PlayerPrefab);

            var player = go.GetComponent<UVRN_Player>();

            player.ApplyProfile(profile);

            NetworkServer.AddPlayerForConnection(conn, go);
            Debug.Log("Instatntiated a new player for connection ID " + conn.connectionId + " with netID " + conn.identity.netId);
        }


        [Client]
        private void Client_OnPlayerCreationFailedResponse(PlayerCreationFailedResponseMessage message)
        {
            Debug.Log("Could not join server: " + message.errorText);
            Client_OnInvalidLogin.Invoke(message.errorText);
            failedAuthMessage = message.errorText;
        }

        // Send player creation request
        [Client]
        public void Client_OnSceneChanged()
        {
            // Client is ready here - set via OnClientSceneChanged in the NetworkManager.

            if (NetworkClient.localPlayer == null)
            {
                if (NetworkClient.connection == null)
                {
                    Debug.LogError("AddPlayer requires a valid NetworkClient.connection.");
                    return;
                }

                if (!NetworkClient.ready)
                {
                    Debug.LogError("AddPlayer requires a ready NetworkClient.");
                    return;
                }

                if (NetworkClient.connection.identity != null)
                {
                    Debug.LogError("NetworkClient.AddPlayer: a PlayerController was already added. Did you call AddPlayer twice?");
                    return;
                }

                SendPlayerCreationRequest();
            }
        }

        [Client]
        private void SendPlayerCreationRequest()
        {
            var playerCreationRequest = new PlayerCreationRequestMessage()
            {
                profile = UVRN_PlayerProfileManager.LocalProfile
            };
            NetworkClient.Send(playerCreationRequest);
            Debug.Log("Sending request for player creation.");
            // TODO conn.identity should be the player object!! so it is not here yet... I cannot use identity.netId...
            // nor conn.connectionId, because the clients don't know client (own or others) IDs
        }

        [Client]
        private void SendActionNotificationToServer(string act)
        {
            var action = new PlayerActionMessage()
            {
                playerID = LocalPlayerID,
                action = act
            };
            NetworkClient.connection.Send(action);
        }

        [Client]
        private void SendInteractionNotificationToServer(uint interactedWithID, string action)
        {
            var interactionMessage = new PlayerInteractionMessage()
            {
                interactorID = LocalPlayerID,
                interacteeID = interactedWithID,
                interaction = action
            };
            NetworkClient.connection.Send(interactionMessage);
        }

        [Client]
        private void Client_OnPlayerLeftMessage(PlayerLeftMessage message)
        {
            Debug.Log("Player left (Player ID: " + message.playerID + ")");
        }

        [Server]
        private void Server_OnPlayerLeavingMessage(NetworkConnection conn, PlayerLeavingMessage message)
        {
            var player = conn.identity.GetComponent<UVRN_Player>();
            var leavingPlayerID = player.ID;
            conn.Disconnect();
            Debug.Log("Player left (Player ID: " + leavingPlayerID + ")");
            var mess = new PlayerLeftMessage { playerID = leavingPlayerID };
            // notify other clients
            NetworkServer.SendToAll(mess);
        }

        [Server]
        private void Server_OnPlayerActionMessage(NetworkConnection conn, PlayerActionMessage message)
        {
            UVRN_Logger.Instance.LogEntry(message.playerID.ToString(), $"Action performed: {message.action}");
        }

        [Server]
        private void Server_OnPlayerInteractionMessage(NetworkConnection conn, PlayerInteractionMessage message)
        {
            UVRN_Logger.Instance.LogEntry(message.interactorID.ToString(), $"Interacting with {message.interacteeID}: {message.interaction}");
        }


        [Client]
        private void Client_OnStop()
        {
            var mess = new PlayerLeavingMessage();
            NetworkClient.connection.Send(mess);
        }
    }
}
