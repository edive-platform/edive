// Created by Vojtech Bruza
using UnityEngine;
using UVRN.Logging;

namespace UVRN.Player.Logging
{
    [RequireComponent(typeof(UVRN_Player))]
    class UVRN_PlayerLogger : UVRN_GenericLogger
    {
        private UVRN_GenericLogger playerHead;
        private UVRN_GenericLogger playerLeftHand;
        private UVRN_GenericLogger playerRightHand;

        public void Init(UVRN_Player player)
        {
            // TODO use custom message for this?
            UVRN_Logger.Instance.LogEntry("Server", $"new player joined; id: {player.ID}; role: {player.Role} name: {player.Username}; color {player.Color}");

            playerHead = SetupTransformChildLogger(player.PlayerModel.headModel);
            playerLeftHand = SetupTransformChildLogger(player.PlayerModel.leftHandModel);
            playerRightHand = SetupTransformChildLogger(player.PlayerModel.rightHandModel);

            string id = player.ID.ToString();
            playerHead.Init(id, "Head", true);
            playerLeftHand.Init(id, "Left Hand", true);
            playerRightHand.Init(id, "Right Hand", true);
            Init(id, "Player", false);
        }

        private UVRN_GenericLogger SetupTransformChildLogger(UVRN_TrackedModelNetworkTransform transform)
        {
            // add loggers to head and hands
            var logger = transform.gameObject.AddComponent<UVRN_GenericLogger>();
            logger.logFrequency = this.logFrequency;
            logger.logPosition = true;
            logger.logRotation = true;
            logger.logLocalScale = false;
            logger.logCustomEvents = this.logCustomEvents;
            return logger;
        }
    }
}