// Created by Vojtech Bruza
using Mirror;
using UnityEngine;

namespace UVRN.Helpers
{
    [RequireComponent(typeof(NetworkTransformBase))]
    public class UVRN_NetworkTransformSyncScale : NetworkBehaviour
    {
        public override void OnStartLocalPlayer()
        {
            Cmd_SyncScale();
        }

        [Command(requiresAuthority = false)]
        public void Cmd_SyncScale(NetworkConnectionToClient sender = null)
        {
            Rpc_SetScale(transform.localScale);
        }

        [Command(requiresAuthority = false)]
        public void Cmd_SetScale(Vector3 scale, NetworkConnectionToClient sender = null)
        {
            transform.localScale = scale;
            Rpc_SetScale(scale);
        }

        [ClientRpc]
        public void Rpc_SetScale(Vector3 scale)
        {
            transform.localScale = scale;
        }
    }
}
