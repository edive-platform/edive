﻿using UnityEngine;

// Made by Vojtěch Brůža
// Inspired by http://wiki.unity3d.com/index.php/Singleton

namespace UVRN.Helpers
{
    /// <summary>
    /// Inherit from this base class to create a singleton.
    /// e.g. public class MyClassName : Singleton<MyClassName> {}
    /// </summary>
    public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        /// <summary>
        /// Check if the singleton instance was already created in the scene.
        /// </summary>
        public static bool Exists
        {
            get
            {
                if (m_Instance == null)
                {
                    // Search for existing instance.
                    var instances = FindObjectsOfType(typeof(T));

                    if (instances.Length > 0)
                    {
                        m_Instance = (T)instances[0];

                        if (instances.Length > 1)
                        {
                            Debug.LogError("[Singleton] Something went really wrong " +
                                " - there should never be more than 1 singleton!" +
                                " Reopenning the scene might fix it.");
                        }

                        Debug.Log("[Singleton] Using instance already created: " +
                            m_Instance.gameObject.name);
                    }
                }
                return m_Instance != null;
            }
        }

        // Check if the singleton is about to be destroyed.
        private static bool m_ShuttingDown = false;
        private static object m_Lock = new object();
        private static T m_Instance;

        /// <summary>
        /// Access singleton instance through this property.
        /// Creates new singleton instance, if it does not already exist in the scene.
        /// The Singleton is not created with DontDestroyOnLoad.
        /// </summary>
        public static T Instance
        {
            get
            {
                if (m_ShuttingDown)
                {
                    Debug.LogWarning("[Singleton] Instance '" + typeof(T) +
                        "' already destroyed. Returning null.");
                    return null;
                }

                lock (m_Lock)
                {
                    if (!Exists)
                    {
                        // Create new instance if one doesn't already exist.
                        // Need to create a new GameObject to attach the singleton to.
                        var singletonObject = new GameObject();
                        m_Instance = singletonObject.AddComponent<T>();
                        singletonObject.name = "[Singleton] " + typeof(T).ToString();

                        Debug.Log("[Singleton] An instance of " + typeof(T) +
                        " is needed in the scene, so '" + singletonObject.name +
                        "' was created.");
                    }

                    // Here the singleton exists for sure.
                    return m_Instance;
                }
            }
        }

        public virtual void Awake()
        {
            // to be able to temporarily destroy an instance and create it again
            m_ShuttingDown = false;
        }

        public virtual void OnApplicationQuit()
        {
            m_ShuttingDown = true;
        }

        public virtual void OnDestroy()
        {
            m_ShuttingDown = true;
        }
    }
}