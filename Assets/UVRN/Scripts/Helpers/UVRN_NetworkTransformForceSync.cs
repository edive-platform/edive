// Created by Vojtech Bruza
using Mirror;
using UnityEngine;
using UnityEngine.Events;

namespace UVRN.Helpers
{
    public class UVRN_NetworkTransformForceSync : NetworkBehaviour
    {
        public void Set_CallCommand()
        {
            Cmd_Set(transform.position, transform.rotation);
        }

        [Command(requiresAuthority = false)]
        public void Cmd_SyncServerToClients(NetworkConnectionToClient sender = null)
        {
            Rpc_Set(transform.position, transform.rotation);
        }

        [Command(requiresAuthority = false)]
        public void Cmd_Set(Vector3 pos, Quaternion rot, NetworkConnectionToClient sender = null)
        {
            transform.position = pos;
            transform.rotation = rot;
            Rpc_Set(pos, rot);
        }

        [ClientRpc]
        public void Rpc_Set(Vector3 pos, Quaternion rot)
        {
            transform.position = pos;
            transform.rotation = rot;
        }
    }
}
