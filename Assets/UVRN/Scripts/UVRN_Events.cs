// Created by Vojtech Bruza
using System;
using UnityEngine.Events;

namespace UVRN
{
    [Serializable]
    public class UnityEventUint : UnityEvent<uint> { }

    [Serializable]
    public class UnityEventString : UnityEvent<string> { }

    [Serializable]
    public class UnityEventInt : UnityEvent<int> { }

    [Serializable]
    public class UnityEventBool : UnityEvent<bool> { }
}