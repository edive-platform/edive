﻿// Created by Vojtech Bruza
using System;
using System.Collections;
using System.Linq;
using Mirror;
using UnityEngine;
using UnityEngine.Events;
using UVRN.Player;

namespace UVRN
{
    /// <summary>
    /// Handles the connection to a specific server (ip:port).
    /// </summary>
    public class UVRN_ConnectToIP : MonoBehaviour
    {
        private UVRN_NetworkManager m_netManager;
        private UVRN_NetworkManager NetworkManager
        {
            get
            {
                if (!m_netManager)
                {
                    m_netManager = (UVRN_NetworkManager)UVRN_NetworkManager.singleton;
                }
                return m_netManager;
            }
        }

        [SerializeField]
        private bool pingBeforeConnect = false;

        [Tooltip("Trying to connect for seconds.")]
        [SerializeField]
        private float pingTimeout = 5;

        private IEnumerator pingAndJoinCoroutine;
        private IEnumerator joinCoroutine;

        /// <summary>
        /// Invoked when not able to reach the server.
        /// </summary>
        public UnityEventString OnFailedToConnect = new UnityEventString();
        public UnityEvent OnTryToConnect = new UnityEvent();

        public void TryToConnect()
        {
            joinCoroutine = WaitAndConnect();
            StartCoroutine(joinCoroutine);
        }

        /// <summary>
        /// Wait till and of frame to correctly finish all invoked events before triggering new.
        /// </summary>
        /// <returns></returns>
        private IEnumerator WaitAndConnect()
        {
            yield return new WaitForEndOfFrame();
            OnTryToConnect.Invoke();
            CheckAndConnect();
        }

        private void CheckAndConnect()
        {
            // wifi
            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                Debug.Log("No internet connection (check device internet conection)...");
                OnFailedToConnect.Invoke("No internet connection (check device internet conection)...");
                return;
            }

            // correct ip
            string ip = NetworkManager.networkAddress;
            if (ip != "localhost" && !ValidateIPv4(ip))
            {
                Debug.Log("Invalid IPv4 address...");
                OnFailedToConnect.Invoke("Invalid IPv4 address...");
                return;
            }

            // should ping the server first?
            if (pingBeforeConnect)
            {
                pingAndJoinCoroutine = PingAndConnect(pingTimeout);
                StartCoroutine(pingAndJoinCoroutine);
            }
            else Connect(ip);
        }

        // TODO move to Utils or somewhere
        private bool ValidateIPv4(string ipString)
        {
            if (string.IsNullOrWhiteSpace(ipString))
            {
                return false;
            }

            string[] splitValues = ipString.Split('.');
            if (splitValues.Length != 4)
            {
                return false;
            }

            byte tempForParsing;

            return splitValues.All(r => byte.TryParse(r, out tempForParsing));
        }

        /// <summary>
        /// Try to ping the server before connecting.
        /// https://docs.unity3d.com/ScriptReference/Ping.html
        /// </summary>
        /// <param name="timeout"></param>
        /// <returns></returns>
        private IEnumerator PingAndConnect(float timeout)
        {
            string ip = NetworkManager.networkAddress;
            Ping p = new Ping(ip);

            float waitTime = .05f;
            WaitForSeconds wfs = new WaitForSeconds(waitTime);

            float time = 0;
            while (!p.isDone && time < timeout)
            {
                yield return wfs;
                time += waitTime;
            }

            if (p.isDone)
            {
                Debug.Log($"IP reached within {time} seconds, connecting...");
                Connect(ip);
            }
            else
            {
                string failMessage = "Could not ping the server after: " + time + " seconds...";
                OnFailedToConnect.Invoke(failMessage);
                Debug.Log(failMessage);
            }
        }

        private void Connect(string ip)
        {
            // If not able to connect
            NetworkManager.Client_OnDisconnectFromServer.AddListener(FailedToConnectCallback);
            // Connect
            NetworkManager.networkAddress = ip;
            NetworkManager.StartClient();
        }

        private void FailedToConnectCallback()
        {
            string failMessage = "Failed to reach the selected server...";
            Debug.Log(failMessage);
            OnFailedToConnect.Invoke(failMessage);
        }

        private void OnEnable()
        {
            // TODO how to find this failed before connecting to the server? rewrite the logging in procedure
            //UVRN_PlayerManager.Client_OnInvalidLogin.AddListener(OnInvalidLogin);
        }

        private void OnDisable()
        {
            if (NetworkManager)
            {
                NetworkManager.Client_OnDisconnectFromServer.RemoveListener(FailedToConnectCallback);
            }
            if (joinCoroutine != null) StopCoroutine(joinCoroutine);
            if (pingAndJoinCoroutine != null) StopCoroutine(pingAndJoinCoroutine);
            //UVRN_PlayerManager.Client_OnInvalidLogin.RemoveListener(OnInvalidLogin);
        }

        //private void OnInvalidLogin(string errorMessage)
        //{
        //    OnFailedToConnect.Invoke(errorMessage);
        //}
    }
}
