﻿// Created by Vojtech Bruza
#if UNITY_ANDROID
using UnityEngine.Android;
#endif
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace UVRN.VoiceChat
{
    public class UVRN_MicAccess : MonoBehaviour
    {
        private IEnumerator waitForMic;

        public void GetVoicePermission(UnityAction<bool> callback)
        {
#if UNITY_ANDROID
            // Init voice chat on the client
            if (!Permission.HasUserAuthorizedPermission(Permission.Microphone))
            {
                Permission.RequestUserPermission(Permission.Microphone);
                waitForMic = WaitForMicPermissionAndSetupVoiceChat(callback);
                StartCoroutine(waitForMic);
            }
        }

        private IEnumerator WaitForMicPermissionAndSetupVoiceChat(UnityAction<bool> callback)
        {
            int countTried = 0;
            int maxTries = 10;
            int secondsBetweenTries = 3;
            while (!Permission.HasUserAuthorizedPermission(Permission.Microphone))
            {
                yield return new WaitForSeconds(secondsBetweenTries);
                if (countTried >= maxTries)
                {
                    Debug.LogError("Permission to use microphone not granted.");
                    StopCoroutine(waitForMic);
                    callback.Invoke(false);
                }
                Debug.Log($"Trying to access microphones. Remaining Atempts: {maxTries - countTried}");
                countTried++;
            }
            Debug.Log("Permission to use microphone granted.");
            callback.Invoke(true);
#endif
        }
    }
}
