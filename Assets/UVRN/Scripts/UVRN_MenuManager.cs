// Created by Vojtech Bruza
using Mirror;
using MUVRE;
using System;
using UnityEngine;
using UVRN.VoiceChat;

namespace UVRN
{
    public class UVRN_MenuManager : MonoBehaviour
    {
        private UVRN_NetworkManager m_uvrn_NetworkManager;
        private UVRN_NetworkManager uvrn_NetworkManager
        {
            get
            {
                if (!m_uvrn_NetworkManager)
                {
                    m_uvrn_NetworkManager = (UVRN_NetworkManager)UVRN_NetworkManager.singleton;
                }
                if (m_uvrn_NetworkManager != UVRN_NetworkManager.singleton)
                {
                    Debug.LogError("Net manager instance changed.");
                }
                return m_uvrn_NetworkManager;
            }
        }

        [SerializeField]
        private GameObject menuVFX;
        [SerializeField]
        private IntSlider voiceChatVolumeSlider;
        [SerializeField]
        private IntSlider micVolumeSlider;

        private void Awake()
        {
            Hide();
        }

        private void OnEnable()
        {
            if (uvrn_NetworkManager) uvrn_NetworkManager.Client_OnConnectToServer.AddListener(OnConnectToServer);
            if (uvrn_NetworkManager) uvrn_NetworkManager.Client_OnDisconnectFromServer.AddListener(OnDisconnectFromServer);

            voiceChatVolumeSlider.onValueChange.AddListener(OnVoiceVolumeChange);
            micVolumeSlider.onValueChange.AddListener(OnMicVolumeChange);
        }

        private void OnDisable()
        {
            if (uvrn_NetworkManager) uvrn_NetworkManager.Client_OnConnectToServer.RemoveListener(OnConnectToServer);
            if (uvrn_NetworkManager) uvrn_NetworkManager.Client_OnDisconnectFromServer.RemoveListener(OnDisconnectFromServer);

            voiceChatVolumeSlider.onValueChange.RemoveListener(OnVoiceVolumeChange);
            micVolumeSlider.onValueChange.RemoveListener(OnMicVolumeChange);
        }

        public void OnConnectToServer()
        {
            Show();
        }

        public void OnDisconnectFromServer()
        {
            Hide();
        }
        private void Show()
        {
            menuVFX.SetActive(true);
        }

        private void Hide()
        {
            menuVFX.SetActive(false);
        }

        private void OnMicVolumeChange(int value)
        {
            uvrn_NetworkManager.VoiceChatManager.SetMicVolume(value);
        }

        private void OnVoiceVolumeChange(int value)
        {
            uvrn_NetworkManager.VoiceChatManager.SetVoiceChatVolume(value);
        }
    }
}
