// Created by Vojtech Bruza
using Mirror;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR.Interaction.Toolkit;
using UVRN.Authorization;
using UVRN.Player;

namespace UVRN.Interactions
{
    public class UVRN_RoleSimpleInteractableObject : NetworkBehaviour
    {
        #region Net Manager
        private UVRN_NetworkManager m_netManager;
        private UVRN_NetworkManager netManager
        {
            get
            {
                if (!m_netManager)
                {
                    m_netManager = (UVRN_NetworkManager)UVRN_NetworkManager.singleton;
                }
                return m_netManager;
            }
        }
        #endregion

        [SerializeField]
        private XRSimpleInteractable simpleInteractable;

        [SerializeField]
        private UVRN_RoleAuthorization auth;

        // TODO make the tooltips visible - these do not show
        [Tooltip("Called both on the server and then on the client when a client interacts with simple interactable")]
        public UnityEvent Net_SelectExited = new UnityEvent();
        [Tooltip("Called on all clients after validated on the server")]
        public UnityEventUint Client_SelectExitedRPC = new UnityEventUint();
        [Tooltip("Called on the same client after validated on the server")]
        public UnityEvent TargetClient_SelectExitedRPC = new UnityEvent();
        [Tooltip("Called on the server when a client interacts with simple interactable")]
        public UnityEvent Server_SelectExited = new UnityEvent();

        #region XRIT Callbacks
        protected virtual void XRIT_OnSelectExited(SelectExitEventArgs args)
        {
            var role = UVRN_PlayerProfileManager.LocalProfile.role;
            if (auth.CanInteract(role)) Cmd_SelectExited(role);
        }
        #endregion

        #region Unity Callbacks
        protected void Awake()
        {
            if (simpleInteractable == null) simpleInteractable = GetComponent<XRSimpleInteractable>();
            if (auth == null) auth = GetComponent<UVRN_RoleAuthorization>();
        }

        private void Reset()
        {
            var rb = GetComponent<Rigidbody>();
            if (rb)
            {
                Debug.LogWarning("Is the rigidbody intentional?");
            }
        }
        #endregion

        #region Mirror Overrides
        public override void OnStartClient()
        {
            simpleInteractable.selectExited.AddListener(XRIT_OnSelectExited);
        }

        public override void OnStopClient()
        {
            simpleInteractable.selectExited.RemoveListener(XRIT_OnSelectExited);
        }
        #endregion

        #region Commands
        [Command(requiresAuthority = false)]
        private void Cmd_SelectExited(string role, NetworkConnectionToClient sender = null)
        {
            // check the role on server
            if (auth.CanInteract(role))
            {
                Net_SelectExited.Invoke();
                Server_SelectExited.Invoke();
                Rpc_SelectExited(sender.identity.GetComponent<UVRN_Player>().ID);
            }
        }
        #endregion

        #region RPCs
        [ClientRpc]
        private void Rpc_SelectExited(uint playerID)
        {
            Net_SelectExited.Invoke();
            Client_SelectExitedRPC.Invoke(playerID);
            // not using target rpc bcause that would mean another rpc call
            if (netManager.PlayerManager && playerID == netManager.PlayerManager.LocalPlayerID) TargetClient_SelectExitedRPC.Invoke();
        }
        #endregion
    }
}
