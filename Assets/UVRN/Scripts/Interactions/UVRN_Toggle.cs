// Created by Vojtech Bruza
using Mirror;
using UnityEngine;
using UnityEngine.Events;

namespace UVRN
{
    public class UVRN_Toggle : NetworkBehaviour
    {
        [SyncVar(hook = nameof(SyncActive))]
        private bool sv_activated = false;

        public UnityEvent Activated = new UnityEvent();
        public UnityEvent Deactivated = new UnityEvent();
        public UnityEventBool Toggled = new UnityEventBool();

        public bool Active => sv_activated;

        private void SyncActive(bool _old, bool _new)
        {
            sv_activated = _new;
            Toggled.Invoke(_new);
            if (sv_activated) Activated.Invoke();
            else Deactivated.Invoke();
        }

        public void Toggle()
        {
            SyncActive(sv_activated, !sv_activated);
        }

        public void SetActive(bool active)
        {
            if (sv_activated != active) SyncActive(sv_activated, active);
        }

        [Command(requiresAuthority = false)]
        public void Cmd_Toggle()
        {
            Toggle();
        }

        [Command(requiresAuthority = false)]
        public void Cmd_SetActive(bool active)
        {
            SetActive(active);
        }

        // Init the callbacks on the client with the value that was set by the server before this method is called
        public override void OnStartClient()
        {
            SyncActive(false, sv_activated);
        }

        // Init the callbacks on the server
        public override void OnStartServer()
        {
            SyncActive(false, sv_activated);
        }
    }
}
