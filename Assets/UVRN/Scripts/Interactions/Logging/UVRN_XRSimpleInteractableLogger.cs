using Mirror;
using UnityEngine;
using UVRN.Logging;

namespace UVRN.Interactions.Logging
{
    [RequireComponent(typeof(UVRN_SimpleInteractableObject))]
    public class UVRN_XRSimpleInteractableLogger : UVRN_GenericLogger
    {
        private UVRN_SimpleInteractableObject simpleInteractable;

        public override void Init()
        {
            Init(null, "Simple Interactable", true);

            simpleInteractable = GetComponent<UVRN_SimpleInteractableObject>();

            simpleInteractable.Server_SelectExitedSender.AddListener(LogObjectInteraction);
        }

        private void LogObjectInteraction(NetworkConnection conn)
        {
            LogCustomMessage("got interacted with by", GetNameFromConnection(conn));
        }
    }
}