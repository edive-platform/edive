﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.Events;

namespace MUVRE
{
    /// <summary>
    /// Component providing the int slider functionality
    /// </summary>
    public class IntSlider : SliderBase
    {
        [Tooltip("The events invoked when interacting with the slider")]
        public UnityEventInt onValueChange;
        /// <summary>
        /// The current value of the slider
        /// </summary>
        public int CurrentValue {
            set => SetValue(value);
            get => currentValue; 
        }
        [Tooltip("The default value of the slider")]
        [SerializeField] private int defaultValue;
        [Tooltip("The minimum value the slider can reach")]
        [SerializeField] protected int minValue = 0;
        [Tooltip("The maximum value the slider can reach")]
        [SerializeField] protected int maxValue = 100;

        public float hapticAmplitude = 0;
        public float hapticDuration = 0;

        [Tooltip("The time it takes to snap the handle to the next position")]
        [SerializeField] private float slideDuration;
        [Tooltip("The range of the snap positions")]
        [SerializeField] private float slack = 0.05f;

        private int currentValue;
        private float interpTime;
        private float realPos = 0;
        private float valPos = 0;
        private float startPos = 0;

        /// <summary>
        /// Invokes the associated event
        /// </summary>
        public override void InvokeEvent() => onValueChange.Invoke(currentValue);

        protected override void Start()
        {
            base.Start();
            SetValue(defaultValue);
        }

        public override void ProcessInteractable(XRInteractionUpdateOrder.UpdatePhase updatePhase)
        {
            if (slideInteractor)
                DetectValueChange();        
        }

        private void Update()
        {
            if (realPos != valPos)
            {
                realPos = Mathf.Lerp(startPos, valPos, interpTime / slideDuration);
                interpTime += Time.deltaTime;
                SetXPosition(realPos);
            }
        }

        private void DetectValueChange()
        {
            float newPosition;
            if (slideInteractor is XRRayInteractor)
                newPosition = Mathf.Clamp(Utils.GetLocalPosition(attachPoint.transform.position, transform).x , xMin, xMax);
            else
                newPosition = Mathf.Clamp(Utils.GetLocalPosition(slideInteractor.transform.position, transform).x + deltaX, xMin, xMax);
            float currValueFloat = Utils.Map(newPosition, xMin, xMax, minValue, maxValue);
            int currValueRound = Mathf.RoundToInt(currValueFloat);
            if (Mathf.Abs(currValueFloat - currValueRound) < slack && currValueRound != currentValue)
            {
                startPos = buttonTransform.localPosition.x;
                realPos = buttonTransform.localPosition.x;
                valPos = Utils.Map(currValueRound, minValue, maxValue, xMin, xMax);
                interpTime = 0;
                onValueChange.Invoke(currValueRound);
                slideInteractor?.GetComponent<XRBaseController>().SendHapticImpulse(hapticAmplitude, hapticDuration);
                currentValue = currValueRound;
            }
        }

        /// <summary>
        /// Sets a new value to the slider
        /// </summary>
        /// <param name="value">The new value</param>
        public void SetValue(int value)
        {
            currentValue = Mathf.Clamp(value, minValue, maxValue);
            SetXPosition(Utils.Map(currentValue, minValue, maxValue, xMin, xMax));
        }      
    }
}