﻿using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace MUVRE
{
    public class XRGrabInteractableExtended : XRGrabInteractable
    {
        [Header("MUVRE")]
        [SerializeField]
        [Tooltip("Offset the attach point to the interaction point")]
        private bool precisionGrab = false;
        [SerializeField]
        private bool hideControllerModel = false;

        protected override void OnSelectEntered(SelectEnterEventArgs args)
        {
            if (hideControllerModel && !(args.interactor is XRSocketInteractor))
                args.interactor.GetComponent<XRBaseController>().modelParent.gameObject.SetActive(false);
            base.OnSelectEntered(args);
            if (precisionGrab && !(args.interactor is XRSocketInteractor)) MatchAttachPointWith(args.interactor);
        }

        private void MatchAttachPointWith(XRBaseInteractor interactor)
        {
            bool hasAttach = attachTransform != null;
            interactor.attachTransform.position = hasAttach ? attachTransform.position : transform.position;
            interactor.attachTransform.rotation = hasAttach ? attachTransform.rotation : transform.rotation;
        }

        protected override void OnSelectExited(SelectExitEventArgs args)
        {
            if (hideControllerModel && !(args.interactor is XRSocketInteractor))
                args.interactor.GetComponent<XRBaseController>().modelParent.gameObject.SetActive(true);
            base.OnSelectExited(args);
            if (precisionGrab && !(args.interactor is XRSocketInteractor)) ResetAttachPoint(args.interactor);
        }

        private void ResetAttachPoint(XRBaseInteractor interactor)
        {
            interactor.attachTransform.localPosition = Vector3.zero;
            interactor.attachTransform.localRotation = Quaternion.identity;
        }

        public void PerformHapticFeedback(XRBaseInteractor interactor, float amplitude, float duration) =>
            interactor.GetComponent<XRBaseController>()?.SendHapticImpulse(amplitude, duration);
    }
}

//Oprava args.interactor - zastaralé!
//namespace MUVRE
//{
//    public class XRGrabInteractableExtended : XRGrabInteractable
//    {
//        [Header("MUVRE")]
//        [SerializeField]
//        [Tooltip("Offset the attach point to the interaction point")]
//        private bool precisionGrab = false;
//        [SerializeField]
//        private bool hideControllerModel = false;

//        protected override void OnSelectEntered(SelectEnterEventArgs args)
//        {
//            if (hideControllerModel && args.interactorObject is XRBaseInteractor interactor && !(interactor is XRSocketInteractor))
//                interactor.GetComponent<XRBaseController>().modelParent.gameObject.SetActive(false);

//            base.OnSelectEntered(args);

//            if (precisionGrab && args.interactorObject is XRBaseInteractor interactor2 && !(interactor2 is XRSocketInteractor))
//                MatchAttachPointWith(interactor2);
//        }

//        private void MatchAttachPointWith(XRBaseInteractor interactor)
//        {
//            bool hasAttach = attachTransform != null;
//            interactor.attachTransform.position = hasAttach ? attachTransform.position : transform.position;
//            interactor.attachTransform.rotation = hasAttach ? attachTransform.rotation : transform.rotation;
//        }

//        protected override void OnSelectExited(SelectExitEventArgs args)
//        {
//            if (hideControllerModel && args.interactorObject is XRBaseInteractor interactor && !(interactor is XRSocketInteractor))
//                interactor.GetComponent<XRBaseController>().modelParent.gameObject.SetActive(true);

//            base.OnSelectExited(args);

//            if (precisionGrab && args.interactorObject is XRBaseInteractor interactor2 && !(interactor2 is XRSocketInteractor))
//                ResetAttachPoint(interactor2);
//        }

//        private void ResetAttachPoint(XRBaseInteractor interactor)
//        {
//            interactor.attachTransform.localPosition = Vector3.zero;
//            interactor.attachTransform.localRotation = Quaternion.identity;
//        }

//        public void PerformHapticFeedback(XRBaseInteractor interactor, float amplitude, float duration) =>
//            interactor.GetComponent<XRBaseController>()?.SendHapticImpulse(amplitude, duration);
//    }
//}
